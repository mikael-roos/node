---
revision:
    "2025-01-04": "(A, mos) First version."
---
![What now?](public/img/linux-what-now.png)

Weekly log - 2025 period 3
==========================

This is an overview of the course setup and what happens week by week.

[[_TOC_]]

<!--
TODO

*

-->



Course round
--------------------------

This course round is in study period 3, spring term, 2025.

These are the dates for this course round.

| What  | Date       | Week | Explanaition     |
|:------|:----------:|:----:|------------------|
| Start | 2025-01-20 |   4  | Course starts    | 
| End   | 2025-03-30 |  13  | Course ends      |

<!--
| Try 2 | 2025-03-30 |  13  | Re-examination   |
| Try 3 | 2025-03-30 |  13  | Rest-examination |
-->



Course elements
--------------------------

This is an overview of the different course elements and when they take place in the course.

| Course element | w01 | w02 | w03 | w04 | w05 | w06 | w07 | w08 | w09 | w10 |
|:--------|:---:|:---:|:---:|:---:|:---:|:---:|:---:|:---:|:---:|:---:|
| 01: Backend development & JavaScript basics |🟩||||||||||
| 02: Asynchronous JavaScript ||🟩|||||||||
| 03: Express Server |||🟩||||||||
| 04: Database and CRUD ||||🟩|||||||
| 05: Security |||||🟩||||||
| 06: GraphQL ||||||🟩|||||
| 07: Docker |||||||🟩||||
| 08: XXX ||||||||🟩|||
| Project: API server |||🟩|🟩|🟩|🟩|🟩|🟩|||
| 09: Practical exam |||||||||🟩||
| 10: Theoretical Exam ||||||||||🟩|



<!--
### Assignment deadlines

This is a general overview of the deadlines to submit the assignments.

| Activity | Deadline | Second deadline | Third deadline |
|:---------|:--------:|:---------------:|:--------------:|
| Project | Friday course week 08 |  |  |

For actual dates you need to look in the course platform or above in the details of the course round.
-->



Prepare yourself
--------------------------

1. Get a bash terminal
    * Linux - you already know where you have it
    * Mac - you have it, start "Terminal"
    * Windows - choose a terminal to work with
        * Git bash (the terminal is included with the [Git installation](https://git-scm.com/))
        * [WSL/WSL2](https://docs.microsoft.com/en-us/windows/wsl/install)
        * (PowerShell might work, but you might need to adapt some commands used in exercises)

1. You may want to view the video "[Use various bash terminals on Windows (Git Bash, (Cygwin), Debian/WSL2)](https://www.youtube.com/watch?v=kialYZs6Oyc&list=PLEtyhUSKTK3gHj087mUjPfXyqMvSy2Rwz&index=1)".

1. You may use [Git](https://git-scm.com/) & GitLab, so you should install it locally.
    * You may want to view the video "[Install Git from the installation program on Windows 10 and configure it](https://www.youtube.com/watch?v=02u7ao7uK5k&list=PLEtyhUSKTK3iTFcdLANJq0TkKo246XAlv&index=1)".

1. Install Node.

1. There will be opportunities to learn how to work with [Docker](https://docs.docker.com/get-docker/), do install it locally and use it in the assignments.



Literature
--------------------------

The following main literature will be used during the course.

1. 



Resources
--------------------------

These resources are useful as a reference.

1) OWASP: https://owasp.org/www-project-top-ten/ 
2) Hacksplaining: https://www.hacksplaining.com/ 
3) Passport: https://www.passportjs.org/
4) JWT: https://jwt.io/
5) Detailed guide OWASP: https://learning.oreilly.com/course/a-detailed-guide/9781837630554/ 
6) Helmet: https://www.npmjs.com/package/helmet 



01: Backend development & JavaScript basics
--------------------------

(Deema)

* Lecture 1: Introduction to Backend Development & JavaScript ES6 Basics
* Lecture 2: Advanced JavaScript for Backend Development



02: Asynchronous JavaScript
--------------------------

(Deema)

* Practical: Node.js - Exploring Asynchronous JavaScript



Skiss
--------------------------

# 03: Express Server

(Lång)

Work through the following code examples.

* [Exercise: Setup an express server for "Hello World"](../../src/express5/hello/README.md)
* [Exercise: Build a JSON REST API server for CRUD](../../src/express5/json-crud/README.md)

# 04: Database and CRUD

(Kort)

Work through the following code examples.

* [Exercise: Connect to a MariaDB (MySQL) database using node and javascript](../../src/express5/database/mariadb/connect/README.md)
* [Exercise: Node menu terminal program doing CRUD to MariaB (MySQL) database using MVC](../../src/express5/database/mariadb/cli-crud/README.md)
* [Exercise: Build a JSON REST API server for CRUD a users collection with a MariaDB (MySQL) database](../../src/express5/database/mariadb/json-crud/README.md)

# 05: Security

(Lång)

* [Lecture: Web application security](../../public/lecture/L12-security/README.md)
* [Exercise: Secure your Express server](../../src/express5/jwt/README-security.md)
* [Exercise: Express with API key for access, authorization, and rate management](../../src/express5/jwt/README-API-key.md)

* [Lecture: Authentication and authorization](../../public/lecture/L0X-authentication-authorization/README.md)
* [Exercise: Express with JSON Web Token (JWT)](../../src/express5/jwt/README.md)

# 06: (GraphQL?)

(Kort)

EJS? Bygg webbsida som genererar utskrift från databasen?

Login med session, flash? Det finns exempelkod som kan byggas på.

Allmän föreläsning om REST API?
Skapa documentation för REST API?
https://swagger.io/tools/swagger-ui/

(Föreläsning GraphQL, litet exempel, frivilligt?)



# 07: Docker

(Lång)

Bygg upp docker container inklusive databas (och nginx?)

Bygga ett litet projekt? Avsluta med en web frontend och en node terminal client?



# 08: Summering & pre inför tentan 

Kort, prep inför tentan?

* [Lecture: Backend summary](../../public/lecture/L0X-backend-summary/slide.html)
* [Code sample: Tiny express server](../../src/express5/tiny/)



# ( Project: API server )

Work 3 days in groups



Project: API server
--------------------------

The mini-project is supposed to be solved over three days and the students can work in groups. The project must contain optional parts so that the ambition and ability of the student groups can allow them to choose the difficulty level of the project.

This assignment is about creating a backend with a REST API based on JSON. You will include an API KEY so only those who use the key can access the server. You will also add JWT authentication for some routes.

The application you are to create is a quiz game that you can adapt to your liking.

The idea of this assignment is to focus on the backend structure of the code and utilities.

Assignment: Create a backend with a REST API with authentication
https://gitlab.com/mikael-roos/node/-/blob/main/assignment/backend-rest-api/README.md

### Current state of JavaScript



03: Express Server
--------------------------

(long)

* Lecture 3: Setting Up an Express Server
* Practical: Express App with Routes, Middleware, and Error Handling

(Intro to Node.js and HTTP)

Working with Docker Containers

Exercises: Create container with Node.js and Express, run simple app in container.


Exercises: Create a simple RESTful API (with CRUD functionality)

<!--
TODO

* 

-->

**Teacher activities**

Lectures with slides:

* 

**Read & Study**

* 

**Work**

* 

**Assignment**

* 



04: Database and CRUD
--------------------------

(short)

* Lecture 4: Database Connectivity and CRUD Operations
* Practical: Add SQL Database Connectivity & CRUD Functionality

Asynchronicity (async/await), connecting to a database
Exercises: Build an app connected to a database, run in container
RESTful API:s

Exercises: Create a simple RESTful API with CRUD functionality



05: Security
--------------------------

(long)

* Lecture 5: Authentication and Authorization
* Practical: Secure CRUD APIs with JWT and Role-Based Access Control

Authentication, authorization, (Oauth 2)
Exercises: Implementing JWT-based authentication and authorization.
Overview of micro services, serverless architecture, web sockets, server-side events



### Lecture 7: Security, OWASP

This lecture is about web application security. We dig into the OWASP Top Ten security issues to learn more about the most common security flaws. We see how this relates to the lists "Common Weakness Enumeration (CWE)" and "Common Vulnerabilities and Exposures (CVE)".

We look through some examples of common attacks and flaws to see how to protect ourselves and the application server.

The "OWASP Juice Shop" is introduced as a playground to learn more about the security aspects of a web application.

Lecture: Web application security
https://gitlab.com/mikael-roos/node/-/tree/main/public/lecture/L12-security



### Exercise 5: Securing your RESTful and GraphQL backends from common vulnerabilities

This exercise consists of two different parts.



### Part 1: Secure your API server

When you have developed your Express web application server, it might be a good suggestion to secure it using some of the best practices for a production server. When working on the development server you want verbosity to help in troubleshooting but when the application is running in production you would want the server and application to run in a more stealth mode.

This exercise will walk you through some of the best practices for securing an Express server.

Exercise: Secure your Express server
https://gitlab.com/mikael-roos/node/-/blob/main/src/express/jwt/README-security.md



### Part 2: Use API key to secure API server

This example shows how to use Express with an API token to protect the JSON API from unauthorized access and usage. The basic idea is that the client requests an API token from the server or the owner of the server. This API token, or API key, is then identifying that specific client. The server (owner) can allow or disallow the token/key, or monitor rates on the usage of the API. 

The client will gain access to the API as long as the API key is supplied on each request and the server accepts the key to be valid. The key can be supplied through the query string, the header, or the body.

Exercise: Express with API key for access, authorization, and rate management
https://gitlab.com/mikael-roos/node/-/blob/main/src/express/jwt/README-API-key.md



### Lecture 8: Authentication, authorization, (Oauth 2)

This lecture is about authentication and authorization in a web application. We explain the terms authentication and authorization and we see how different (access versus role-based) control lists can be used to manage who can access what resource. 

The secrets need to be securely stored, both in the database and in the repository.

We wrap up by reviewing the technique for OAuth, API keys and JSON Web Token.

Lecture: Authentication and authorization
https://gitlab.com/mikael-roos/node/-/blob/main/public/lecture/L0X-authentication-authorization/README.md



### Exercise 6: Implementing JWT-based authentication and authorization

This example shows how to use Express with JSON Web Token for authentication. The JWT token is generated by the server when the user requests it by using a user and a password to authenticate.

The user and the passwords are encrypted using bcrypt. The example does not use a data source to store the user and password.

Exercise: Express with JSON Web Token (JWT)
https://gitlab.com/mikael-roos/node/-/blob/main/src/express/jwt/README.md



06: (GraphQL)
--------------------------

(short)

* Lecture 6: GraphQL Integration
* Practical: Add a GraphQL Layer for Data Access

GraphQL
Exercises: Create a simple GraphQL API
Security, OWASP
Exercises: Securing your RESTful and GraphQL backends from common vulnerabilities




07: (Docker)
--------------------------

(long)

* Lecture 7: Introduction to Docker and Deployment
* Practical: Containerize Backend with Docker



08: Current state of JavaScript
--------------------------

(short)

_Recapitulation, best practices, future trends_

This lecture is about the various techniques, tools, frameworks and platforms that make up the JavaScipt ecosystem.

Lecture: The state of JavaScript and its eco system
https://mikael-roos.gitlab.io/node/lecture/L13-state-of-js/slide.html



09: Practical exam
--------------------------

Extra Session: Preparation for Exam



10: Theoretical Exam
--------------------------

Theoretical Exam: Backend Development Concepts

