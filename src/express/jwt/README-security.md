---
revision: 
    "2024-03-04": "(A, mos) First version."
---
Exercise: Secure your Express server
==============================

<!-- ![done](.img/done.png) -->

When you have developed your Express web application server, it might be a good suggestion to secure it using some of the best practices for a production server. When working on the development server you want verbosity to help in troubleshooting but when the application is running in production you would want the server and application to run in a more stealth mode.

This exercise will walk you through some of the best practices for securing an Express server.


[[_TOC_]]


<!--
TODO

* This exercise is not about application level security for xss and csrf (injection, sql injection), that needs its own exercise.
  * The article has some suggestions in "Additional considerations"
* Add some reading guidelines
* Include how to setup TLS, Nginx and certificates (may require own server)

-->



<!--
Video
-----------------------------

This is a recorded presentation, 18 minutes long (English), when Mikael goes through the content of this article.

[![2023-02-13 en](https://img.youtube.com/vi/wM1RPbxamC4/0.jpg)](https://www.youtube.com/watch?v=wM1RPbxamC4)
-->



Expectations on this exercise
-----------------------------

You should already have your own express server where you can continue to work with. 

The idea is that you can work through this exercise and try out the examples and study the code. At the end of the exercise you should have secured your Express server with the examples shown.

This exercise is partly built on the article from Express documentation "[Production Best Practices: Security](https://expressjs.com/en/advanced/best-practice-security.html)".



Start the server
-----------------------------

This example includes an Express server to work with. You can start the server locally on your machine or using docker-compose.

The example server has all the security improvements added to it, so you can use it as a case study.



### Clone the repo

You can clone the whole exercise repo like this.

```
git clone https://gitlab.com/mikael-roos/node.git
cd node
```

This particular exercise lies in the directory `src/express/jwt`.

```
cd src/express/jwt
```



### Start the server locally

Install the dependencies:

```
npm install
```

Start the server:

```
npm start
```

Open a web browser and connect to it using: `http://127.0.0.1:3000/`.

You can optionally start the server on another port like this.

```
PORT=3001 npm start
```



### Start the server using docker-compose

You can build the image and start the container like this.

```
# Build the server
docker-compose build server

# In the foreground
docker-compose up server

# In the background, detached
docker-compose up -d server
```

Open a web browser and connect to it using: `http://127.0.0.1:3000/`.

You can stop the container using `ctrl-c` (foreground) or with `docker-compose down` (background).



Use the latest version of the server
-----------------------------

There exists many older versions of express but it would be preferred to use the latest version that should be most up to date with security fixes.

To ensure that you have the latest version you can read the "[Release Change Log](https://expressjs.com/en/changelog/4x.html)" on the website.

You can also check the status and version that is published in the [npm packet repository for Express](https://www.npmjs.com/package/express). Verify what version is the latest.

You can also check what version you have installed locally using `npm list`.

```
$ npm list express
express-jwt@1.0.0 /home/mos/git/mos/course/node/src/express/jwt
└── express@4.18.2
```

You can run the command `npm outdated` to see if there are any packages that can be updated.

```
$ npm outdated
Package          Current  Wanted  Latest  Location                      Depended by
express           4.18.2  4.18.3  4.18.3  node_modules/express          jwt
express-session   1.17.3  1.18.0  1.18.0  node_modules/express-session  jwt
nodemon           2.0.22  2.0.22   3.1.0  node_modules/nodemon          jwt
```

You can then do a `npm update` to update the packages where it is possible.

On my machine the express packages was updated by `npm update`. To verify the status we do another `npm outdated`.

```
$ npm outdated
Package  Current  Wanted  Latest  Location              Depended by
nodemon   2.0.22  2.0.22   3.1.0  node_modules/nodemon  jwt
```

For this package it seems to be a later version, but the constraints in the `package.json` prevents it from being upgraded.

```json
  "devDependencies": {
    "nodemon": "^2.0.20"
  }
```

I update the `package.json` to the latest version, just to show how its done.

```json
  "devDependencies": {
    "nodemon": "^3.1.0"
  }
```

Then I do another `npm update` followed by an `npm outdated` which now displays en empty output and no more packages are to be updated.

The final place to look for the status of the software is on the [GitHub repository for Express](https://github.com/expressjs/express) where we can see details of the releases and information and progress of current development. Many projects has a [history logg](https://github.com/expressjs/express/blob/master/History.md) to show the details of the releases. YOu might also find interesting details in the issues where troubles and features might be reported and the status of development is displayed.



Use https and TLS
-----------------------------

We will not implement this in this exercise, but the recommendation is to use Transport Layer Security (TLS) as a layer of security to avoid attacks like [package sniffing](https://en.wikipedia.org/wiki/Packet_analyzer) and [man-in-the-middle](https://en.wikipedia.org/wiki/Man-in-the-middle_attack).

It might be common practice to add an installation of the Nginx web server infront of your Express application server. You can then configure Nginx to use TLS and a guide for that is "[Mozilla wiki: Security/Server Side TLS](https://wiki.mozilla.org/Security/Server_Side_TLS#Recommended_Server_Configurations)".

When you need certificates you can use "[Let's Encrypt](https://letsencrypt.org/about/)" which is a free service that supplies server certificates.



Use Helmet
-----------------------------

Helmet helps you secure the Express app by setting values in the HTTP response headers. Check out the [website for helmet.js](https://helmetjs.github.io) to learn more on what this package does.

You can install the [package helmet](https://www.npmjs.com/package/helmet) using npm.

```
npm install helmet
```

It is easy to get going and it requires no configuration to be used.

```
import helmet from "helmet";

const app = express();

// Use Helmet!
app.use(helmet());
```

You can see the difference to the HTTP response.

This is without using Helmet.

```
$ curl --head  http://localhost:3000/
HTTP/1.1 200 OK
X-Powered-By: Express
Accept-Ranges: bytes
Cache-Control: public, max-age=0
Last-Modified: Sat, 20 Jan 2024 12:23:57 GMT
ETag: W/"2e7-18d26d3e64e"
Content-Type: text/html; charset=UTF-8
Content-Length: 743
Date: Mon, 04 Mar 2024 16:20:56 GMT
Connection: keep-alive
Keep-Alive: timeout=5
```

This is how the header looks like when using helmet.

```
$ curl --head  http://localhost:3000/
HTTP/1.1 200 OK
Content-Security-Policy: default-src 'self';base-uri 'self';font-src 'self' https: data:;form-action 'self';frame-ancestors 'self';img-src 'self' data:;object-src 'none';script-src 'self';script-src-attr 'none';style-src 'self' https: 'unsafe-inline';upgrade-insecure-requests
Cross-Origin-Opener-Policy: same-origin
Cross-Origin-Resource-Policy: same-origin
Origin-Agent-Cluster: ?1
Referrer-Policy: no-referrer
Strict-Transport-Security: max-age=15552000; includeSubDomains
X-Content-Type-Options: nosniff
X-DNS-Prefetch-Control: off
X-Download-Options: noopen
X-Frame-Options: SAMEORIGIN
X-Permitted-Cross-Domain-Policies: none
X-XSS-Protection: 0
Accept-Ranges: bytes
Cache-Control: public, max-age=0
Last-Modified: Sat, 20 Jan 2024 12:23:57 GMT
ETag: W/"2e7-18d26d3e64e"
Content-Type: text/html; charset=UTF-8
Content-Length: 743
Date: Mon, 04 Mar 2024 16:21:39 GMT
Connection: keep-alive
Keep-Alive: timeout=5
```

To actually understand what these HTTP headers will help us with we need to learn more on these headers and the Helmet docs contain details on what each header does. You can also learn how to configure each header and how to disable some headers.



Use non custom error handlers
-----------------------------

Express sends its own formatted details for a 404 Not Found and 500 Error messages. This can be used to understand that there is an Express server behind the application and you can prevent that by creating your own custom handlers.

```js
// Add these right before app.listen()

// custom 404
app.use((req, res, next) => {
  res.status(404).send("Not found")
})

// custom error handler
app.use((err, req, res, next) => {
  console.error(err.stack)
  res.status(500).send('Something is broke')
})
```

An alternative is to put the error functions into a module and add them like this to the server configuration.

```js
// Error handler
app.use(errorHandler.notFoundDefault)
app.use(errorHandler.errorDefault)
```

Either way, building your own error handlers will prevent the attacker from using this to learn about the Express server.


<!--
Use cookies securly
-----------------------------

This is needed when you work with cookies and sessions. If you do not use cookies or sessions you can just skip this step.

Take care when setting the cookies and the session cookie. There are two middlewares that can help you setting the cookies and the session.

* [express-session](https://www.npmjs.com/package/express-session)
* [cookie-session](https://www.npmjs.com/package/cookie-session)

A middleware function is a filter that the request must pass to reach its final route handler. We can attach a function that verifies the API key as a middleware function to any route.
-->


<!--
Prevent brute-force attacks against authorization
-----------------------------

Ensure your dependencies are secure
-----------------------------

Snyk

Avoid other known vulnerabilities
-----------------------------

Snyk
-->



Summary
-----------------------------

This exercise showed you a few security measures that you can take in securing you Express application server.

You can learn about more security measures in the article "[Production Best Practices: Security
](https://expressjs.com/en/advanced/best-practice-security.html)"
