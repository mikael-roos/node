---
revision: 
    "2024-03-04": "(A, mos) First version."
---
Exercise: Express with API key for access, authorization, and rate management
==============================

![done](.img/done.png)

This example shows how to use Express with an API token to protect the JSON API from unauthorized access and usage. The basic idea is that the client requests an API token from the server or the owner of the server. This API token, or API key, is then identifying that specific client. The server (owner) can allow or disallow the token/key, or monitor rates on the usage of the API. 

The client will gain access to the API as long as the API key is supplied on each request and the server accepts the key to be valid. The key can be supplied through the query string, the header, or the body.

[[_TOC_]]


<!--
TODO

* Rename try1 to try5 with other names
* Rewrite the verify() code to raise exceptions
* Discuss best practice when implementing an API key
  * What data to store with it
  * How to register for it
* Add part discussing the rate limits

-->



<!--
Video
-----------------------------

This is a recorded presentation, 18 minutes long (English), when Mikael goes through the content of this article.

[![2023-02-13 en](https://img.youtube.com/vi/wM1RPbxamC4/0.jpg)](https://www.youtube.com/watch?v=wM1RPbxamC4)
-->



Expectations on this exercise
-----------------------------

You should already have your own express server where you can continue to work with. 

The idea is that you can work through this exercise and try out the examples and study the code.

The exercise walks you through how to implement the API key through:

* the query string,
* the header,
* or the body.

At the end of the exercise you should be able to implement all three variants into a single middleware.

The code supplied with this exercise contains parts of the solutions and you can study the code to learn how its built.



Start the server
-----------------------------

This example includes an Express server to work with. You can start the server locally on your machine or using docker-compose.



### Clone the repo

You can clone the exercise repo like this.

```
git clone https://gitlab.com/mikael-roos/node.git
cd node
```

This particular exercise lies in the directory `src/express/jwt`.

```
cd src/express/jwt
```



### Start the server locally

Install the dependencies:

```
npm install
```

Start the server:

```
npm start
```

Open a web browser and connect to it using: `http://127.0.0.1:3000/`.

You can optionally start the server on another port like this.

```
PORT=3001 npm start
```



### Start the server using docker-compose

You can build the image and start the container like this.

```
# Build the server
docker-compose build server

# In the foreground
docker-compose up server

# In the background, detached
docker-compose up -d server
```

Open a web browser and connect to it using: `http://127.0.0.1:3000/`.

You can stop the container using `ctrl-c` (foreground) or with `docker-compose down` (background).



Clients
-----------------------------

When you work through this exercise you can do it using a client like Postman.

You can also create your own client using a bash-script with curl.

Or you can create a client with JavaScript and fetch. Here follows an example on creating a client in JavaScript with fetch.

```js
const url = `http://localhost:${process.env.PORT || 3000}/api/v1/apikey/list`
const options = {
  method: 'GET',
}

console.log(url)

let response = await fetch(url, options)
let data = await response.json()

console.log(response.status)
console.log(data)
console.log(response.headers)
```

The above client is available in `script/apikey/list.js` and you can run it like this.

```
node script/apikey/list.js
```

Start it like this if you are running the server on another port.

```
PORT=3001 node script/apikey/list.js
```



What API keys are available?
-----------------------------

This is a nice server that let you choose the API key that you can use. You can ask the server to find out what API keys that are available through this route.

```
GET /api/v1/apikey/list
```

Run the script that accesses the above route and prints the response.

```
node script/apikey/list.js 
```

It can look like this when you execute it in the terminal.

```
$ node script/apikey/list.js 
http://localhost:3000/api/v1/apikey/list
200
[
  {
    description: 'Master key with unlimited usage',
    key: '8040462fc9c65d3ad045eec6bf994e8d',
    rate: null,
    usage: 0
  },
  {
    description: 'Trial key with limited usage',
    key: '21bc55a1a71abe81d1f141e80a745587',
    rate: 3,
    usage: 0
  }
]
```

It seems like there are two API keys available to choose from, one has a rate limit on 3 requests and the other seems to have an unlimited rate. 



Use the API key with a GET request
-----------------------------

The following route is protected and it can not be accessed without a API key.

```
GET /api/v1/apikey/try1
```

You can try to access the route using portman or curl.

```
$ curl http://localhost:3000/api/v1/apikey/try1
{"type":"forbidden","message":"You have not supplied a valid API key!"}
```

You can add the API key as part of the query string, like this.

```
GET /api/v1/apikey/try1?API_KEY=<the key>
```

You can use any of the keys you listed above and you can try it using postman or curl.

```
$ curl http://localhost:3000/api/v1/apikey/try1?API_KEY=21bc55a1a71abe81d1f141e80a745587
{"message":"YES. You supplied a valid key through the query string!"}
```

There is an example script you can use to try this out further, just add the API_KEY as an argument to the script.

It looks like this.

```
$ node script/apikey/try1.js 21bc55a1a71abe81d1f141e80a745587
http://localhost:3000/api/v1/apikey/try1?API_KEY=21bc55a1a71abe81d1f141e80a745587
200
{ message: 'YES. You supplied a valid key through the query string!' }
```

The server has now verified that it trusts the key and that your usage is within the rate allowed. It will then carry out the actual work behind the route.

On the server side you can retrieve the API key from the query string like this.

```js
controller.verifyQueryString = (req, res) => {
  const aKey = req.query.API_KEY || null

  // code to verify that aKey is valid

  res.json({
    message: 'YES. You supplied a valid key through the query string!'
  })
}
```

Exactly how to verify the API key depends on how you store it on the server. Usually you check that the API key matches any of API keys stored in the database. 

If you need a smaller example to start with, then you can do something like this and use a hard coded key.

```js
controller.verifyQueryString = (req, res) => {
  const aKey = req.query.API_KEY || null

  // code to verify that aKey is valid
  if (aKey === 'my-secret-key') {
    return res.status(403).json({
      type: 'forbidden',
      message: 'You have not supplied a valid API key!'
    })
  }

  res.json({
    message: 'YES. You supplied a valid key through the query string!'
  })
}
```

Later on you might want to move the checking of the API key into its own function or module and perhaps store the API keys in the database. This exercise will however not focus on that part.



Use the API key with a POST request
-----------------------------

When working with POST, PUT, PATCH, DELETE, you will most likely work with supplying parts of the request as the body in the HTTP request.

Then it might be suitable to also supply the API key as part of the body, or perhaps as part of the HTTP header. Both ways seems valid.

Lets see how that can be done.



### Supply API key through header

When supplying the API key through the header you set a header value to bear the API key. It can look like this when using a node client with fetch.

```js
const url = `http://localhost:${process.env.PORT || 3000}/api/v1/apikey/try2`
const options = {
  method: 'POST',
  headers: {
    Authorization: API_KEY
  }
}
let response = await fetch(url, options)
``` 

You may decide on what header field to use for the API key, here I used the key `Authorization`.

You can use the client program `try2.js` to verify that it works.

```
$ node script/apikey/try2.js 21bc55a1a71abe81d1f141e80a745587
http://localhost:3000/api/v1/apikey/try2
200
{ message: 'YES. You supplied a valid key through the header!' }
```

The server verified that it got a valid API key.

On the server side you can extract the API key from the header like this.

```js
controller.verifyHeader = (req, res) => {
  const aKey = req.header('Authorization') || null

  // code to verify that aKey is valid

  res.json({
    message: 'YES. You supplied a valid key through the header!'
  })
}
```

See if you can add this test to your own server, just to try it out.



### Supply API key through body

There is also the option to supply the API key throgh the body. A node client that does that can look like this.

```js
const url = `http://localhost:${process.env.PORT || 3000}/api/v1/apikey/try3`
const body = {
  authorization: API_KEY
}
const options = {
  method: 'POST',
  headers: {
    'Content-Type': 'application/json'
  },
  body: JSON.stringify(body)
}

let response = await fetch(url, options)
```

This is how it can look like when we run the client program `try3.js`.

```
$ node script/apikey/try3.js 21bc55a1a71abe81d1f141e80a745587
http://localhost:3000/api/v1/apikey/try3
200
{ message: 'YES. You supplied a valid key through the body!' }
```

On the server side it is just a matter of extracting the API key from the body.

```js
controller.verifyBody = (req, res) => {
  const aKey = req.body.authorization || null

  // code to verify that aKey is valid

  res.json({
    message: 'YES. You supplied a valid key through the body!'
  })
}
```

You see that it looks pretty much the same, even if the API key is supplied through the body, header or querystring. Perhaps we could rewrite the code into a single function?



Check API key as middleware
-----------------------------

A middleware function is a filter that the request must pass to reach its final route handler. We can attach a function that verifies the API key as a middleware function to any route.



### The actual controller (route) action

To try it out we create a route that gives an important answer.

```js
/**
 * Provide a magic answer.
 *
 * @param {object} req Express request object.
 * @param {object} res Express response object.
 */
controller.magicAnswer = (req, res) => {
  res.json({
    message: 'YES. The magic answer is 42!'
  })
}
```

I'll add this controller action to the following route and without any protection.

```js
router.post('/try4', controller.magicAnswer)
```

The route is now like this.

```
GET /api/v1/apikey/try4
```

To try it out we use postman or curl or through the script `try4.js`.

```
$ node script/apikey/try4.js
http://localhost:3000/api/v1/apikey/try4
200
{ message: 'YES. The magic answer is 42!' }
```

Ok, so far so good. Now lets add some protection.



### Create a middleware for API key

We can create one single function that checks if the API key is supplied through either the query string, the header or the body. Then we add that function as a protection layer, a middleware, before the actual controller (route handler) action is called.

The middleware function can look like this.

```js
/**
 * Verify that the API key exists in the query string.
 *
 * @param {object} req Express request object.
 * @param {object} res Express response object.
 * @param {object} next Express next object.
 */
export default (req, res, next) => {
  const aKey = req.query.API_KEY 
    || req.header('Authorization')
    || req.body.authorization
    || null

  // Code to verify that aKey is a valid API key
  // Throw error if not, or return a reponse of 403

  next()
}
```

We then add it as a middleware to the route, like this, for both a GET and a POST route.

```js
router.get('/try5', verifyApikey, controller.magicAnswer)
router.post('/try5', verifyApikey, controller.magicAnswer)
```

Now we are ready to try it out.



### Try the middleware using the query string

To verify that it works there is a client `apikey_qs.js` that supply the API key through the query string. You can try it out like this.

```
$ node script/apikey/try_qs.js 8040462fc9c65d3ad045eec6bf994e8d 
http://localhost:3000/api/v1/apikey/try5?API_KEY=8040462fc9c65d3ad045eec6bf994e8d
200
{ message: 'YES. The magic answer is 42!' }
```

Supply another API key to see it fail.

```
$ node script/apikey/try_qs.js BAD-API-KEY
http://localhost:3000/api/v1/apikey/try5?API_KEY=BAD-API-KEY
403
{
  type: 'forbidden',
  message: 'You have not supplied a valid API key!'
}
```



### Try the middleware using the header

To verify that it works there is a client `apikey_header.js` that supply the API key through the header. You can try it out like this.

```
$ node script/apikey/try_header.js 8040462fc9c65d3ad045eec6bf994e8d 
http://localhost:3000/api/v1/apikey/try5
200
{ message: 'YES. The magic answer is 42!' }
```

Supply another API key to see it fail.

```
$ node script/apikey/try_header.js BAD-API-KEY
http://localhost:3000/api/v1/apikey/try5
403
{
  type: 'forbidden',
  message: 'You have not supplied a valid API key!'
}
```



### Try the middleware using the body

To verify that it works there is a client `apikey_body.js` that supply the API key through the body. You can try it out like this.

```
$ node script/apikey/try_body.js 8040462fc9c65d3ad045eec6bf994e8d 
http://localhost:3000/api/v1/apikey/try5
200
{ message: 'YES. The magic answer is 42!' }
```

This was a POST request, the other two were GET requests. However, that does not matter with respect to the middleware function.

Supply another API key to see it fail.

```
$ node script/apikey/try_body.js BAD-API-KEY
http://localhost:3000/api/v1/apikey/try5
403
{
  type: 'forbidden',
  message: 'You have not supplied a valid API key!'
}
```



Own work
-----------------------------

Ensure that you can add the features of an API key to your own node server. That way you can assure that you know many of the bits and pieces of dealing with the API key.

On the server side this feature actually boils down to a middleware function to extract the API key from the query string, the header or the body. Then we off course need to add details on how to verify the API key with for example a set of API keys in the database.



Summary
-----------------------------

This example showed how to work with an API key and various ways of suppling it from the client to the server.
