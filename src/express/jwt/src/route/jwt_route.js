/**
 * Routes for the JWT example.
 */
import express from 'express'
import { controller } from '../controller/jwt_controller.js'
import { jwtMiddleware } from '../middleware/jwt.js'

export const router = express.Router()

router.get('/list', controller.list)
router.post('/register', controller.register)
router.post('/login', controller.login)
router.get('/token', jwtMiddleware.jwtTokenIsValid, controller.token)
