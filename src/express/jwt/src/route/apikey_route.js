/**
 * Routes for the API key example.
 */
import express from 'express'
import { controller } from '../controller/apikey_controller.js'
import verifyApikey from '../middleware/apikey.js'

export const router = express.Router()

router.get('/list', controller.list)
router.get('/try1', controller.verifyQueryString)
router.post('/try2', controller.verifyHeader)
router.post('/try3', controller.verifyBody)
router.get('/try4', controller.magicAnswer)
router.get('/try5', verifyApikey, controller.magicAnswer)
router.post('/try5', verifyApikey, controller.magicAnswer)
