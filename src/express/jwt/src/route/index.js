/**
 * One place to mount all routes for the application.
 */
import express from 'express'
import { router as apikeyRoute } from './apikey_route.js'
import { router as jwtRoute } from './jwt_route.js'
import { router as helloRoute } from './hello_route.js'

export const router = express.Router()

router.use('/api/v1/apikey', apikeyRoute)
router.use('/api/v1/jwt', jwtRoute)
router.use('/hello', helloRoute)
