/**
 * Middleware for JWT token.
 */
import http from 'http'
import { users } from '../model/users.js'

const middleware = {}
export { middleware as jwtMiddleware }

/**
 * Verify that the JWT token exists in the request.
 *
 * @param {object} req Express request object.
 * @param {object} res Express response object.
 * @param {object} next Express next object.
 */
middleware.jwtTokenIsValid = (req, res, next) => {
  const token = req.header('Authorization') ||
    null

  try {
    res.locals.jwt = users.verifyToken(token)
    console.log(res.locals.jwt)
  } catch (error) {
    console.error(error)
    const err = new Error(http.STATUS_CODES[403])
    err.status = 403
    throw err
  }

  next()
}
