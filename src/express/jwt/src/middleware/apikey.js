import { apikey } from '../model/apikey.js'

/**
 * Verify that the API key exists in the request.
 *
 * @param {object} req Express request object.
 * @param {object} res Express response object.
 * @param {object} next Express next object.
 */
export default (req, res, next) => {
  const aKey = req.query.API_KEY ||
    req.header('Authorization') ||
    req.body.authorization ||
    null

  // This part could be rewritten to throw errors and not send a response
  if (!apikey.verifyKey(aKey)) {
    return res.status(403).json({
      type: 'forbidden',
      message: 'You have not supplied a valid API key!'
    })
  } else if (!apikey.verifyRate(aKey)) {
    return res.status(403).json({
      type: 'forbidden',
      message: 'You have reached your usage rate!'
    })
  }

  next()
}
