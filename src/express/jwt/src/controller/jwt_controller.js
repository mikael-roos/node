/**
 * Controller actions for the JWT example.
 */
import { users } from '../model/users.js'

export const controller = {}

/**
 * List all the users.
 *
 * @param {object} req Express request object.
 * @param {object} res Express response object.
 */
controller.list = (req, res) => {
  res.setHeader('Content-Type', 'application/json')
  res.end(JSON.stringify(users.getAll(), null, 2))
}

/**
 * Register a new user.
 *
 * @param {object} req Express request object.
 * @param {object} res Express response object.
 */
controller.register = async (req, res) => {
  const username = req.body.username
  const password = req.body.password
  const user = users[username] ?? null

  if (users.get(username)) {
    return res.status(409).json({
      type: 'failed',
      message: 'The user already exists and can not be registered!'
    })
  }

  await users.add(username, password)
  res.json({
    type: 'success',
    message: 'The user was registered.',
    user: {
      username
    }
  })
}

/**
 * Perform a login and generate a JWT.
 *
 * @param {object} req Express request object.
 * @param {object} res Express response object.
 */
controller.login = async (req, res) => {
  const username = req.body.username
  const password = req.body.password

  const jwtToken = await users.login(username, password)
  if (!jwtToken) {
    return res.status(401).json({
      type: 'failed',
      message: 'Wrong user or password!'
    })
  }

  res.json({
    type: 'success',
    message: 'The user was authenticated.',
    user: {
      username,
      role: users.getRole(username)
    },
    token: jwtToken
  })
}

/**
 * Get the details in the JWT token.
 *
 * @param {object} req Express request object.
 * @param {object} res Express response object.
 */
controller.token = async (req, res) => {
  res.json({
    type: 'success',
    message: 'The JWT token was validated.',
    payload: res.locals.jwt
  })
}
