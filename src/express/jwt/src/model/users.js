import bcrypt from 'bcrypt'
import jwt from './jwt.js'

import { readFile } from 'node:fs/promises'
import path from 'node:path'
import { fileURLToPath } from 'node:url'

const __dirname = path.dirname(fileURLToPath(import.meta.url))
const usersPath = path.join(__dirname, '../../data/users.json')
const usersDb = JSON.parse(await readFile(usersPath, 'utf8'))

export const users = {}

/**
 * Get all users.
 *
 * @returns {object} Details on all the users.
 */
users.getAll = () => {
  const res = []

  for (const user in usersDb) {
    res.push({
      username: user,
      plainPassword: usersDb[user].plainPassword
    })
  }

  return res
}

/**
 * Get details on a specific user.
 *
 * @param {string} username Username of the user.
 * @returns {object} Details on the user.
 */
users.get = (username) => {
  return usersDb[username]
}

/**
 * Add a user to the database.
 *
 * @param {string} username Username of the user.
 * @param {string} password Plaintext password of the user.
 */
users.add = async (username, password) => {
  const saltRounds = 10

  usersDb[username] = {
    username,
    bcryptPassword: await bcrypt.hash(password, saltRounds)
  }
  console.log('New user was registered with the details:')
  console.log(usersDb[username])
}

/**
 * Authenticate the user and return a JWT upon success.
 *
 * @param {string} username The username.
 * @param {string} password The password.
 * @returns {[string,boolean]} A JWT token if login succedded, else false.
 */
users.login = async (username, password) => {
  const user = usersDb[username] ?? null

  if (user) {
    const hashedPassword = user.bcryptPassword
    const success = await bcrypt.compare(password, hashedPassword)

    if (success) {
      return jwt.getJwtToken(username, users.getRole(username), hashedPassword)
    }
  }

  return false
}

/**
 * Verify the user and the JWT token.
 *
 * @param {string} token The JWT token.
 * @returns {object} The content of the JWT token.
 */
users.verifyToken = (token) => {
  const payload = jwt.getPayload(token)
  const username = payload.username ?? null
  const user = usersDb[username] ?? null
  const hashedPassword = user.bcryptPassword ?? null

  return jwt.verify(token, hashedPassword)
}
