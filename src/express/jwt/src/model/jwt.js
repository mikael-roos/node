import jwt from 'jsonwebtoken'

const model = {}
export default model

/**
 * Generate a JWT token.
 *
 * @param {string} username Username of the user.
 * @param role
 * @param secret
 * @returns {object} Details on the user.
 */
model.getJwtToken = async (username, role, secret) => {
  const payload = {
    username,
    role
  }
  const token = jwt.sign(payload, secret)
  return token
}

/**
 * Parse and extract the payload of the JWT token.
 *
 * @param {string} token JWT token.
 * @returns {object} Details saved in the token.
 */
model.getPayload = (token) => {
  return JSON.parse(Buffer.from(token.split('.')[1], 'base64').toString())
}

/**
 * Verify the JWT token and its signature.
 *
 * @param {string} token JWT token.
 * @param {string} secret The secret.
 * @returns {object} The content of the verified token.
 */
model.verify = (token, secret) => {
  return jwt.verify(token, secret)
}
