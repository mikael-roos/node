const controller = require('../controllers/gameController')

const express = require('express')
const router = express.Router()

router.get('/', controller.index)
router.get('/init', controller.init)
router.get('/play', controller.play)
router.get('/cheat', controller.cheat)
router.post('/guess', controller.guess)

module.exports = router
