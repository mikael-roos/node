const express = require('express')
const router = express.Router()

router.get('/', (req, res, next) => {
  const data = {

  }
  res.render('form/get', data)
})

router.get('/process', (req, res, next) => {
  const data = {
    today: new Date().toUTCString(),
    incoming: req.query,
    incomingJSON: JSON.stringify(req.query, null, 2)
  }
  res.render('form/get-result', data)
})

module.exports = router
