const express = require('express')
const router = express.Router()

router.get('/', (req, res, next) => {
  const data = {

  }
  res.render('form/post-no-redirect', data)
})

router.post('/process', (req, res, next) => {
  const data = {
    today: new Date().toUTCString(),
    incoming: req.body,
    incomingJSON: JSON.stringify(req.body, null, 2)
  }
  res.render('form/post-result-no-redirect', data)
})

module.exports = router
