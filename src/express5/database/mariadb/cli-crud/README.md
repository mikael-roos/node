---
revision: 
    "2025-02-09": "(A, mos) First version."
---
![showff](.img/show-users.png)

Exercise: Node menu terminal program doing CRUD to MariaB (MySQL) database using MVC
==============================

This example shows how to create a node terminal program that is menu driven. You perform CRUD operations towards a MariaDB (MySQL) database to create, update, read and delete users from a database table. The code is structured according to the MVC (Model, View, Controller) design pattern.

The exercise walks you through the bits and pieces of a working example program.

[[_TOC_]]

<!--
TODO

* 

-->



<!--
Video
-----------------------------

This is a recorded presentation, 18 minutes long (English), when Mikael goes through the content of this article.

[![2023-02-13 en](https://img.youtube.com/vi/wM1RPbxamC4/0.jpg)](https://www.youtube.com/watch?v=wM1RPbxamC4)
-->



Preconditions
-----------------------------

You have performed the exercise "[Connect to a MariaDB (MySQL) database using node and javascript](../connect/README.md)".

This exercise builds upon the work done in that exercise.



Get going with this example program
-----------------------------

There are two ways to get going with this example program to make it run on your own computer. The first is to clone the example repo and use the code there. The other is to create the code, step by step, in your own setup.

Here is how to get going by cloning the repo.

```
git clone https://gitlab.com/mikael-roos/node.git
cd node

cd src/express5/database/mariadb/cli-crud
npm install
```

Now you can access all the parts of the example code "to make it your own".



MVC design pattern
-----------------------------

MVC stands for Model-View-Controller. It's a design pattern used in software development to separate an application into three interconnected components:

* Model: Manages data and business logic.
* View: Displays the data to the user.
* Controller: Handles user input and updates the Model and View accordingly.

This separation helps in organizing code, making it easier to manage and scale.

The view part in this cli-application, will deal with input and output to and from the terminal. The idea is to organise all code around input/output to the views.



Code structure
-----------------------------

Here is the overview of the code structure.

![structure](.img/structure.png)

_Figure. The various code part of this example program._

Another way to review the code structure is to check what how the directory structure look like.

```
├── app.js
└── src
    ├── Menu.js
    ├── config
    │   └── database.js
    ├── controller
    │   └── UsersController.js
    ├── model
    │   └── UsersModel.js
    ├── service
    │   └── DatabaseService.js
    └── view
        ├── InputHandler.js
        └── OutputHandler.js
``` 

The code structure above is choosen to look like the MVC design pattern. The view directory holds the input/output with the terminal.

Here follows a walk through of the different parts of this code.



The start in `app.js`
-----------------------------

The application starts in `app.js` and can be started like this.

```
npm start
```

or 

```
node --env-file=.env app.js
```

It starts by creating the database connection and to start the menu program that is executed in a loop.

```js
import databaseService from "./src/service/DatabaseService.js"
import menu from "./src/Menu.js"

// Connect to the database
await databaseService.connect()

// Do the menu loop
do {
  menu.show()
} while (await menu.handleInput())

// End the application
await databaseService.closeConnection()
process.exit(0)
```

The application is shut down and the database connection is closed once the user is done with the menu.

Review the source code for [`app.js`](./app.js).



The Menu class
-----------------------------

The menu class acts as a router, based on the user selection from the menu. The menu can be printed and the user makes a selection of the various menu choices.

The menu uses the `inputHandler` to get details from the user and then sends them to the selected action in the `usersController`.

This way the menu class is implemented to use the MVC design pattern. The `inputHandler` is perhaps not an actual view, but is stored in that directory to gather all input and output from/to the user.

<details>
<summary>Source code for the Menu class.</summary>

```js

import userController from "./controller/UsersController.js"
import inputHandler from "./view/InputHandler.js"

/**
 * Class to handle the menu and user interactions.
 * Acts like a router to direct user actions.
 * @class
 */
class Menu {
  /**
   * Show the menu to the user.
   */
  show() {
    console.log("\n--- Main Menu ---")
    console.log("1. Show users")
    console.log("2. Add user")
    console.log("3. Update user")
    console.log("4. Delete user")
    console.log("5. Exit")
  }

  /**
   * Handle user input and route to the appropriate controller method.
   * @async
   */
  async handleInput() {
    const userInput = await inputHandler.ask("Select an option: ")

    switch (userInput) {
    case "1":
      await userController.showUsers()
      break

    case "2":
      const nameToAdd = await inputHandler.ask("Enter username: ")
      const emailToAdd = await inputHandler.ask("Enter email: ")
      const passwordToAdd = await inputHandler.ask("Enter password: ")
      await userController.addUser(nameToAdd, emailToAdd, passwordToAdd)
      break

    case "3":
      const idToUpdate = parseInt(
        await inputHandler.ask("Enter ID to update: "),
        10
      )
      const newName = await inputHandler.ask("Enter new name: ")
      const newEmail = await inputHandler.ask("Enter new email: ")
      await userController.updateUser(idToUpdate, newName, newEmail)
      break

    case "4":
      const idToDelete = parseInt(
        await inputHandler.ask("Enter ID to delete: "),
        10
      )
      await userController.deleteUser(idToDelete)
      break

    case "5":
      console.log("Exiting the application...")
      return false
      break

    default:
      console.log("Invalid option. Please try again.")
    }
    return true
  }
}

export default new Menu()
```

</details>

Review the source code for [`src/Menu.js`](./src/Menu.js).



The InputHandler
-----------------------------

The `inputHandler` deals with reading input from the user through the terminal.

```js
import readline from "readline/promises"
import { stdin as input, stdout as output } from "process"

/**
 * Handles user input in the terminal.
 * @class
 */
class InputHandler {
  /**
   * Ask a question to the user and return the response.
   * @async
   * @param {string} question - The question to ask.
   * @returns {Promise<string>} The user's input.
   */
  async ask(question) {
    const rl = readline.createInterface({ input, output })
    const answer = await rl.question(question)
    rl.close()
    return answer
  }
}

export default new InputHandler()
```

This inputHandler is implemented using the builtin node feature `readline`.

Review the source code for [`src/view/InputHandler.js`](./src/view/InputHandler.js).



The controller `UsersController`
-----------------------------

The controller class handles the callback for each menu action. The controller is dependent on the model class to carry out the actual work. Each controller action uses the model and then directs to the view for output to the terminal. The controller uses the `outputHandler` for output.

<details>
<summary>Source code for the controller.</summary>

```js
import usersModel from "../model/UsersModel.js"
import outputHandler from "../view/OutputHandler.js"

/**
 * Controller to perform CRUD fo rhte users collection.
 * @class
 */
class UsersController {
  /**
   * Show all users by fetching data from the model and displaying it via the output handler.
   * @async
   */
  async showUsers() {
    try {
      const users = await usersModel.getAllUsers()
      outputHandler.showUsers(users)
    } catch (error) {
      outputHandler.showError("Could not fetch users.")
    }
  }

  /**
   * Add a new user to the database.
   * @async
   * @param {string} name - The name of the user.
   * @param {string} email - The email of the user.
   * @param {string} password - The password for the user.
   */
  async addUser(name, email, password) {
    try {
      const id = await usersModel.addUser(name, email, password)
      outputHandler.showSuccess(`New user added with ID: ${id}`)
    } catch (error) {
      outputHandler.showError("Could not add user.", error)
    }
  }

  /**
   * Update an existing user in the database.
   * @async
   * @param {number} id - The ID of the user to update.
   * @param {string} name - The new name of the user.
   * @param {string} email - The new email of the user.
   */
  async updateUser(id, name, email) {
    try {
      const success = await usersModel.updateUser(id, name, email)
      if (success) {
        outputHandler.showSuccess(`User with ID ${id} updated.`)
      } else {
        outputHandler.showError(`No user found with ID ${id}.`)
      }
    } catch (error) {
      outputHandler.showError("Could not update user.", error)
    }
  }

  /**
   * Delete a user from the database.
   * @async
   * @param {number} id - The ID of the user to delete.
   */
  async deleteUser(id) {
    try {
      const success = await usersModel.deleteUser(id)
      if (success) {
        outputHandler.showSuccess(`User with ID ${id} deleted.`)
      } else {
        outputHandler.showError(`No user found with ID ${id}.`)
      }
    } catch (error) {
      outputHandler.showError("Could not delete user.", error)
    }
  }
}

export default new UsersController()
```

</details>

Review the source code for [`src/controller/UsersController.js`](./src/controller/UsersController.js).



The OutputHandler
-----------------------------

In the `outputHandler` we gather the code that prints out the result to the user in the terminal.

```js
/**
 * Handles displaying output to the user via the terminal.
 * @class
 */
class OutputHandler {
  /**
   * Show the list of users.
   * @param {Array} users - The list of users to display.
   */
  showUsers(users) {
    console.table(users)
  }

  /**
   * Display a success message.
   * @param {string} message - The success message to display.
   */
  showSuccess(message) {
    console.log(`\n✅ ${message}`)
  }

  /**
   * Display an error message.
   * @param {string} message - The error message to display.
   */
  showError(message, error) {
    console.error(`\n❌ ${message}`)
    if (error) {
      console.error(`\n${error}`)
    }
  }
}

export default new OutputHandler()
```

Review the source code for [`src/view/OutputHandler.js`](./src/view/OutputHandler.js).



The model `UsersModel`
-----------------------------

The `usersModel` prepares the database request and executes them and then return the results (to the controller). One could say that the `usersModel` is the glue to the database connection.

Here is how it can look for getting all the users.

```js
class UsersModel {
  /**
   * Get all users from the database.
   * @async
   * @returns {Promise<Array>} An array of users.
   */
  async getAllUsers() {
    const query = "SELECT * FROM user"
    return await databaseService.query(query)
  }
}
```

<details>
<summary>Source code for the model.</summary>

```js
import databaseService from "../service/DatabaseService.js"

/**
 * Model to interact with the 'users' table in the database.
 * @class
 */
class UsersModel {
  /**
   * Get all users from the database.
   * @async
   * @returns {Promise<Array>} An array of users.
   */
  async getAllUsers() {
    const query = "SELECT * FROM user"
    return await databaseService.query(query)
  }

  /**
   * Add a new user to the database.
   * @async
   * @param {string} name - The name of the user.
   * @param {string} email - The email of the user.
   * @param {string} password - The passowrd of the user.
   * @returns {Promise<number>} The ID of the newly created user.
   */
  async addUser(name, email, password) {
    const query = "INSERT INTO user (username, email, password) VALUES (?, ?, ?)"
    const result = await databaseService.query(query, [name, email, password])
    return result.insertId
  }

  /**
   * Update an existing user in the database.
   * @async
   * @param {number} id - The ID of the user to update.
   * @param {string} name - The new name of the user.
   * @param {string} email - The new email of the user.
   * @returns {Promise<boolean>} True if the update was successful, false otherwise.
   */
  async updateUser(id, name, email) {
    const query = "UPDATE user SET username = ?, email = ? WHERE id = ?"
    const result = await databaseService.query(query, [name, email, id])
    return result.affectedRows > 0
  }

  /**
   * Delete a user from the database.
   * @async
   * @param {number} id - The ID of the user to delete.
   * @returns {Promise<boolean>} True if the deletion was successful, false otherwise.
   */
  async deleteUser(id) {
    const query = "DELETE FROM user WHERE id = ?"
    const result = await databaseService.query(query, [id])
    return result.affectedRows > 0
  }
}

export default new UsersModel()
```

</details>

Review the source code for [`src/model/UsersModel.js`](./src/model/UsersModel.js).



The database service
-----------------------------

The code that connects to the database and executes database queries are gathered into the `DatabaseService` class which is a singleton meaning it is one instance that is available to all that wants to connect to the database.

The database service holds a single connection to the database which is reused for all queries.

<details>
<summary>Source code for the model.</summary>

```js
import mysql from "mysql2/promise"
import { config } from "../config/database.js"

/**
 * Service (singleton) to manage database operations.
 * @class
 */
class DatabaseService {
  #connection = null

  /**
   * Connect to the database using the configuration provided.
   * @async
   * @returns {Promise<void>}
   */
  async connect() {
    try {
      this.#connection = await mysql.createConnection(config)
    }
    catch (error) {
      console.error(error)
    }

    process.on("SIGINT", () => this.closeConnection())
    process.on("SIGTERM", () => this.closeConnection())
  }

  /**
   * Execute a database query.
   * @async
   * @param {string} queryString - The SQL query string.
   * @param {Array} [params=[]] - The parameters for the query.
   * @returns {Promise<Object>} The result of the query.
   * @throws {Error} If the query execution fails.
   */
  async query(queryString, params = []) {
    try {
      const [rows] = await this.#connection.execute(queryString, params)
      return rows
    } catch (error) {
      throw new Error(`Database error: ${error.message}`)
    }
  }

  /**
   * Close the database connection pool.
   * @async
   */
  async closeConnection() {
    try {
      await this.#connection.end()
      console.log("Database connection closed.")
    } catch (err) {
      console.error("Error closing the database connection:", err.message)
      throw err
    }
  }
}

export default new DatabaseService()
```

</details>

Review the source code for [`src/service/DatabaseService.js`](./src/service/DatabaseService.js).

The configuration details for the database are stored in a separate file `config/database.js`.

Review the source code for [`src/config/database.js`](./src/config/database.js).



Add more features to the example program
-----------------------------

To really make this code "your own" you need to use it and one way is to try and implement more features into it. Here are some suggestions of features to implement.

Doing so will help you get a better understanding of the code and how it works.



### View a single user

Add a menu choice that shows only one users. You need to input the id of the user you want to see the details for.



### Search for users

Add a menu choice that lets you search for through the users to find the ones that match your search string. The search string is read from the user through the terminal.



### Delete all users

Add a menu choice to delete all users.



Summary
-----------------------------

You have now walked through an example programs that shows how to implement a menu-drivven program in a MVC-like manner connecting to a database service.

