---
revision: 
    "2025-02-06": "(A, mos) First version."
---
![showff](.img/table.png)

Exercise: Connect to a MariaDB (MySQL) database using Node and Javascript
==============================

This example shows how to connect to a MySQL database, or a MariaDB database, from an Node application.

[[_TOC_]]

<!--
TODO

* Exercise on how to install the database (using docker) (own exercise)
* Add dotenv to prevent hardcoding in source files
* move database connection to services/DatabaseService.js
* Add more table packages for more examples on output
  * Add description on how to print single row in table (exampe exists in import)

* Use terminal windows for application?
* Split into two exercises

-->



<!--
Video
-----------------------------

This is a recorded presentation, 18 minutes long (English), when Mikael goes through the content of this article.

[![2023-02-13 en](https://img.youtube.com/vi/wM1RPbxamC4/0.jpg)](https://www.youtube.com/watch?v=wM1RPbxamC4)
-->



Get going with this example program
-----------------------------

There are two ways to get going with this example program to make it run on your own computer. The first is to clone the example repo and use the code there. The other is to create the code, step by step, in your own setup.

Here is how to get going by cloning the repo.

```
git clone https://gitlab.com/mikael-roos/node.git
cd node

cd src/express5/database/mariadb/connect
npm install
```

Now you can access all the parts of the example code "to make it your own".



The database MySQL
-----------------------------

You need to have installed the database MariaDB (MySQL), and you need to have access to it using a terminal client `mariadb` (`mysql`), for example like this.

```
mariadb -umaria -pM@ria -hlocalhost
```

The important part is that you know how to connect to the database and what credentials to use.

In this example I will be using the following credentials.

```
user: maria
password: M@ria
host: localhost
```

You can [review the SQL](./sql/create-user-maria.sql) for how to create a user like that.

Lets also create a database to use for this example.

```sql
CREATE DATABASE IF NOT EXISTS `maria`;
USE maria;
```

When its all done it can look like this.

![mariadb terminal](.img/mariadb-terminal.png)

_Figure. Using the terminal to connect to the mariadb database server and the database maria._

Now lets move on and create a js-file that can connect to the database like this.



Create a table
-----------------------------

To be able to use the database we create a table for users with some details.

```sql
--
-- Create a sample table for users and add some users to it.
--
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
    `id` INT AUTO_INCREMENT PRIMARY KEY,
    `username` VARCHAR(50) NOT NULL UNIQUE,
    `password` VARCHAR(255) NOT NULL,
    `email` VARCHAR(100) NOT NULL UNIQUE,
    `created_at` TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    `updated_at` TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
);

INSERT INTO user (username, password, email) VALUES 
    ('alice', 'password123', 'alice@example.com'),
    ('bob', 'securepass', 'bob@example.com'),
    ('charlie', 'mypassword', 'charlie@example.com'),
    ('diana', '1234abcd', 'diana@example.com'),
    ('eve', 'passw0rd!', 'eve@example.com')
;
```

The source for this SQL is in [`sql/create-user-table.sql`](./sql/create-user-table.sql).

It can look like this when you are done.

![user table](.img/table-user.png)

_Figure. A table 'user' exists and has some rows into it._



Install the database connection library
-----------------------------

To connect from node to the database we need a library like [`mysql2`](https://www.npmjs.com/package/mysql2) that supports both MySQL and MariaDB. Lets install that.

```
npm install mysql2
```

There might be other libraries out there that does the same, fel free to try other variants.



Connect to the database using js
-----------------------------

To connect to the database we create a small script [`src/connect.js`](./src/connect.js).

At first we try to do that script as small as possible, just to verify that it works.

```js
// Get the client
import mysql from 'mysql2/promise';

// Create the connection to database
const connection = await mysql.createConnection({
  host: 'localhost',
  user: 'maria',
  password: 'M@ria',
  database: 'maria',
});

// A simple SELECT query
try {
  const [results, fields] = await connection.query(
    'SELECT * FROM `user` WHERE `username` = "alice"'
  );

  console.log(results); // results contains rows returned by server
  console.log(fields);  // fields contains extra meta data about results, if available
} catch (err) {
  console.log(err);
}

// Close the database connection
await connection.end();
console.log('Database connection closed.');
```

Inspect the code and then execute it.

```
node src/connect
```

You should se the program working and that it outputs details on the selected user.

It can look like this when you are done.

![node connect](.img/node-connect.png)

_Figure. A printout of detals from the user table._



Prepared statements
-----------------------------

When working with databases we prefer to use prepared statements. We rewrite the previous example program to use that instead. The code can be saved in [`src/prepared.js`](./src/prepared.js).

```js
import mysql from 'mysql2/promise';

// create the connection to database
const connection = await mysql.createConnection({
  host: 'localhost',
  user: 'maria',
  password: 'M@ria',
  database: 'maria',
});

try {
  // execute will internally call prepare and query
  const sql = 'SELECT * FROM `user` WHERE `username` = ?'
  const args = ['alice']
  const [results] = await connection.execute(sql, args)

  // results contains rows returned by server
  console.log(results); 
} catch (err) {
  console.log(err);
}

// Close the database connection
await connection.end();
console.log('Database connection closed.');
```

In the mysql2 library, the `execute()` is a wrapper that calls both `prepare()` and `query()`. So we will be fine, as long as we use the placeholder `?` and send in the `args` separately. Prepared statements are usually the safest way to prevent SQL injections with databases.



Move connection to its own module
-----------------------------

Lets move the repetetive code of connecting to the database to its own module so our main program has less code in it.

Create the file [`config/database.js`](./src/config/database_novenv.js).

<details>
<summary>Code for the database module.</summary>

Add this code to the file `config/database.js`.

```js
/**
 * A centralized module for managing a single MySQL database connection.
 * Provides methods to initialize, retrieve, and close the connection.
 */
import mysql from 'mysql2/promise';

let connection = null; // Store the connection instance

/**
 * Initializes the database connection if not already initialized.
 * @returns {Promise<mysql.Connection>} The active database connection.
 */
const initializeConnection = async () => {
  if (!connection) {
    connection = await mysql.createConnection({
      host: 'localhost',
      user: 'maria',
      password: 'M@ria',
      database: 'maria',
    });
    console.log('Database connected.');
  }
  return connection;
};

/**
 * Retrieves the active database connection. If not initialized, it will call `initializeConnection`.
 * @returns {Promise<mysql.Connection>} The active database connection.
 */
const getConnection = async () => {
  if (!connection) {
    await initializeConnection();
  }
  return connection;
};

/**
 * Closes the database connection if it exists and resets the connection instance to `null`.
 * @returns {Promise<void>}
 */
const closeConnection = async () => {
  if (connection) {
    await connection.end();
    connection = null;
    console.log('Database connection closed.');
  }
};

/**
 * The exported `db` object, containing methods to manage the database connection.
 * @property {Function} initialize - Initializes the database connection.
 * @property {Function} getConnection - Retrieves the active database connection.
 * @property {Function} close - Closes the active database connection.
 */
const db = {
  initialize: initializeConnection,
  getConnection,
  close: closeConnection,
};

export default db;
```

Walk through the code and try to understand it before you continue.

</details>



Now we update the main program and put this into the file `src/import.js`.

```js
import db from './config/database.js';

// Connect the database and get the 'connection'
const con = await db.getConnection();

// Perform a query
const sql = 'SELECT * FROM `user` WHERE `username` = ?';
const args = ['alice'];
const [results] = await con.execute(sql, args);

// Print the resultset
console.log(results);

// Close the database connection
await db.close();
```

Verify that it works like expected by running it.

```
node src/import
```

It can look like this.

![database import](.img/import.png)

_Figure. It looks the same but now the code is structured into modules._



Use an `.env` file for the database connection details
-----------------------------

Currently we have hard-coded credentials that we use to connect to the database. It is better if we put these credentials into an `.env` file, like this.

Create the `.env` file and save the following in it.

```bash
DB_HOST='localhost'
DB_USER='maria'
DB_PASSWORD='M@ria'
DB_SCHEMA='maria'
```

This means that we can update the connection part of the `src/config/database.js`.

It looks like this now.

```js
    connection = await mysql.createConnection({
      host: 'localhost',
      user: 'maria',
      password: 'M@ria',
      database: 'maria',
    });
    console.log('Database connected.');
```

Lets update it to look like this, using the environment variables from `.env` instead.

```js
    connection = await mysql.createConnection({
      host: process.env.DB_HOST,
      user: process.env.DB_USER,
      password: process.env.DB_PASSWORD,
      database: process.env.DB_SCHEMA,
    });
```

Let node know where to load the environment, when you start the program.

```
node --env-file=.env src/import
```

You should get the same output as before.

You can always change some detail in the `.env` file and run the program again to verify that it actually works.



Print data in a table
-----------------------------

We can do a "SELECT * FROM user" and display the results in a table using `console.table()`.

Lets create a new file `src/table.js` and put the following code into it.

```js
import db from './config/database.js';

// Connect the database and get the 'connection'
const con = await db.getConnection();

// Perform a query
const sql = 'SELECT * FROM `user`';
const args = [];
const [results] = await con.execute(sql, args);

// Print the resultset
console.table(results);

// Close the database connection
await db.close();
```

Now execute the program.

```
node --env-file=.env src/import
```

Running it will look like this. The result is printed into a table output.

![database table](.img/table.png)

_Figure. All the rows in the resultset are printed into a table like output._



Summary
-----------------------------

You have now walked through some example programs on how to connect to a MySQL/MariaDB database from node javascript.
