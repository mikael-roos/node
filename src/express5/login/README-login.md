---
revision: 
    "2025-03-02": "(C, mos) Updated to express 5 and integrated with database."
    "2023-02-13": "(B, mos) Updated with MJS and improved structure in README."
    "2021-11-31": "(A, mos) First version."
---
![login](.img/login-alice.png)

Express with authentication and login
==============================

This example shows how to use Express to enable login using authentication and the session. As frontend we use HTML pages rendered by the server using EJS template language. The user and the passwords are encrypted using bcrypt. The example uses a database table used in previous exercises.

[[_TOC_]]


<!--
TODO

* Flash message can not survive double redirect, for example double POST redirect
* Add public routes for user profile

-->

<!--
Video
-----------------------------

This is a recorded presentation, 18 minutes long (English), when Mikael goes through the content of this article.

[![2023-02-13 en](https://img.youtube.com/vi/wM1RPbxamC4/0.jpg)](https://www.youtube.com/watch?v=wM1RPbxamC4)
-->



Preconditions
-----------------------------

You have worked through the [README.md introduction](./README.md) and you know how to start the example program.

You have worked through the example programs [Express with session and flash messages](./README-session.md) and [Express with Create, Read, Update, Delete (CRUD)](./README-crud.md).



Start the server
-----------------------------

Start the server.

```
npm run dev
```

Open a web browser and connect to it using: `http://127.0.0.1:3000/`.

Use the example through this start route:

* `/user/login`

![login](.img/login.png)

_Figure. Here is where the example starts, ready to login._

Now you are ready to start to work.



Dependencies in this example
-----------------------------

This example adds several parts to the code base. All parts marked with green is updated, or added, in this example.

![dependency](.img/dependencies.png)

_Figure. These are the dependencies in this example and the green parts are updated._

Here is a short summary of the updates for this example.

| File | Description |
|------|-------------|
| `express.js` | Add the middleware to extract details from the authenticated user into `res.locals`. |
| `route/index.js` | Add the new routes. |
| `route/authenticateRoute.js` | The routes to authenticate the user and show the user profile. |
| `route/adminRoute.js` | The routes for the protected admin area. |
| `controller/AuthenticateController.js` | The controller actions for the login, profile and logout routes. |
| `controller/AdminController.js` | The controller actions for the admin part. |
| `model/UsersModel.js` | The model was slightly updated with new method to check the user and password and extract details of the user, of user & password matched. |
| `middleware/injectLocals.js` | Inject details of the authenticated user into `res.locals` so the view can access them. |
| `middleware/authenticationMiddleware.js` | Nwe middleware to protect routes through middleware. |

Then the following view directories were added to fulfill the example.

| File | Description |
|------|-------------|
| `views/authenticate` | Template files for the login, profile and logout part. |
| `views/admin` | Template files for the admin part. |

Now we should be ready to try out and review the example.

Here is the content of those two view directories.

```
src/views/admin/   
├── users_table.ejs
└── view_all.ejs   

src/views/authenticate/     
├── header.ejs              
├── login.ejs               
└── profile.ejs             
```

As you see, there are not that many views that are needed for this example.



Login
-----------------------------

You can now try to login as any of the users you have in the database. Try the user "alice" with password "alice" or as the user 'admin' with the user 'admin'.

The following routes are mainly involved in this.

```js
router.get('/login', controller.login)
router.post('/login', controller.loginPost)
```

It looks like this when you try to login.

![login alice](.img/login-alice.png)

_Figure. Try to login as the user Alice._

If you enter a non existing user or a wrong password, then you will be redirected back to the login form and a flash message is shown.

![login fail](.img/login-fail.png)

_Figure. The flash message shows an error that the user and password did not match._

It is called a flash message since it is only shown once and the message will go away if you reload the page.

This is how it looks like when you do a successful login, you are redirected to the users personal profile.

![login success](.img/login-success.png)

_Figure. The login was successful and a redirect was made to the profile page._

Lets see how the session stores the data of an authenticated user.



Session keep details on authenticated user
-----------------------------

When a user is authenticated, the details on that user is stored in the session. That is how the express server kan keep track of the authenticated user.

![session authenticated](.img/session-authenticated.png)

_Figure. The session contains details of the authenticated user._

The data that the session holds on the authenticated user is the following.

```js
  "authenticated": {
    "username": "alice",
    "role": "user"
  }
```

It is up to the application to decide what data to store in the session, for the authenticated user.



Update navbar when user is authenticated
-----------------------------

The navbar is updated when a user is autheticated, it changes from this.

```
Login
```

To this.

```
[ alice ] Profile | Logout
```

To make this happen the view needs to be aware of if the user is authenticated or not and then render the page dynamically. The template file `authenticate/header.ejs` looks like this.

```html
<h1>Authentication of users with session</h1>

<nav>
    <% if (!authenticated) { %> 
    <a href="./user/login">Login</a> 
    <% } else { %>
    <span>[ <%= authenticated.username %> ]</span> 
    <a href="./user/profile">Profile</a> |
    <a href="./user/logout">Logout</a>
        <% if (authenticated.role === 'admin') { %>
        | <a href="./admin">Admin</a>
        <% } %>
    <% } %>
</nav>
```

It is EJS if-statements that decide what the navbar should look like, and it works on the variable `authenticated`.

The data comes from the session, but to expose it to the view we need to add it to `res.locals` and that is done in a middleware `localsMiddleware.authenticatedUser` that looks like this.

```js
/**
 * Check if user is authenticated and add to locals.
 *
 * @param {object} req Express request object.
 * @param {object} res Express response object.
 * @param {object} next Express next object.
 */
middleware.authenticatedUser = (req, res, next) => {
  res.locals.authenticated = req.session?.authenticated
  next()
}
```

The middleware reads the data from the session and adds it to the `locals` so the views get access to it.



Profile
-----------------------------

When a user is authenticated they can view their profile. The following route is added to support that.

```js
router.get('/profile', auth.isAuthenticatedOrRedirectLogin, controller.profile)
```

It can look like this.

![user profile](.img/user-profile.png)

_Figure. The user profile is displayed to the authenticated user._

The profile template page renderes the details on the authenticated user.

In the route you might noticed that the route was protected by a middleware `auth.isAuthenticatedOrRedirectLogin`. That means that the middleware must be passed, before the actual controller action `controller.profile` is processed. This is a way to protect controller actions through middleware.

The implementation of the middleware is in the file `middleware/authenticationMiddleware.js` looks like this.

```js
/**
 * Check if user is authenticated or redirect to login.
 *
 * @param {object} req Express request object.
 * @param {object} res Express response object.
 * @param {object} next Express next object.
 */
middleware.isAuthenticatedOrRedirectLogin = (req, res, next) => {
  const username = req.session?.authenticated?.username

  if (!username) {
    req.session.flashMessage = 'You need to authenticate to  access the resource!'
    return res.redirect('./login')
  }
  next()
}
```

The middleware checks if there is an authenticated user and if it is not, then it redirects to the login page with a flash message.

It can look like this if a user tries to access the profile route `user/profile` without being authenticated.

![user profile no access](.img/login-profile-no-authenticated.png)

_Figure. The user profile can only be accessed by an authenticated user._

Using middleware like this, is a good way to protect resources on the server.



Logout
-----------------------------

The route that performs a logout is doing so by resetting the parts in the session, that relates to the authenticated user.

![logou user](.img/logout.png)

_Figure. When the user is performing a logout they are redirected to the login page._

This is how the controller action looks like for the logout part.

```js
  /**
   * Logout the user.
   *
   * @param {object} req Express request object.
   * @param {object} res Express response object.
   */
  async logout (req, res) {
    delete req.session.authenticated
    req.session.flashMessage = 'User is logged out!'
    res.redirect('./login')
  }
```

We can check in the session that the data was removed.

![session logout](.img/logout-session.png)

_Figure. The session data for "authenticated" is removed when the user is logged out._

That was the basics for how to implement authentication using the session.



Admin role
-----------------------------

A user is connected to a role in the database. That role can be used to check what privilegies that user has. Some parts of the website can be protected and anly allow access to, for example, users having the role "admin".

To show this, we login as the user Alice and then we try to access a resource that needs the role as admin.

![no access admin](.img/admin-no-access.png)

_Figure. The user does not have access to the route `/admin`._

To achieve this we set up the route to the admin area like this.

```js
router.get('/', auth.hasRoleAdminOrForbidden, controller.viewAllUsers)
```

We protect the actual controller action `controller.viewAllUsers` with the middleware `auth.hasRoleAdminOrForbidden`.

This is how we implement the middleware.

```js
/**
 * Check if user is authenticated and role is admin.
 *
 * @param {object} req Express request object.
 * @param {object} res Express response object.
 * @param {object} next Express next object.
 */
middleware.hasRoleAdminOrForbidden = (req, res, next) => {
  const role = req.session?.authenticated?.role

  if (role !== 'admin') {
    const error = new Error('You need to have role=admin to access this resource!')
    error.status = 403
    throw error
  }
  next()
}
```

The middleware checks if there is an authenticated user and if it has the role of "admin", otherwise it throws and error with the status code 403 which means "forbidden".

On the other hand, if the user has the correct role, the result will be displayed.

First we login as a user having the role of "admin". It can look like this and now you also see that the navbar displayed an extra item, the link to "Admin". It is the if-statments in the view that does this.

![admin welcome](.img/admin-welcome.png)

_Figure. The admin user has login and can then access the link to the "Admin" panel._

Clicking on the "Admin" link takes us to the admin panel, but only if we have the correct role for it.

![admin panel](.img/admin-panel.png)

_Figure. The admin panel is displayed, here it shows all users as an example admin panel._

This is how you can protect parts of the website using details from the authenticated user and middleware to check.



<!--
Debug view data
-----------------------------

It might help to understand how the `res.data` is populated with data from the middleware by checking the debug route `user/data` that shows the content of the `res.data` as populated by the middlewares.

![data](.img/data.png)
-->



Review the implementation
-----------------------------

You know have a rough overview on how an implementation of authentication might look like when implementing it through server side rendered web pages using EJS views and storing the authenticated user in the session.



### Own work, enhance the example

Choose some of the following features to enhance the example and "make it your own".

* [ ] Add a feature so a new user can register themselves. Add the link "Register new user" on the login page and implement it.
* [ ] Add a middleware that ensures that only authenticated users can perform a logout. You decide what should happen if the user is not authenticated.
* [ ] Implement the "index" route `users/` and it should redirect an authenticated user to the profile page and an unauthenticated user to the login page.
* [ ] On the user profile page, add so the user can change their password.
* [ ] Add full crud onto the `admin/` section and protect it.



Summary
-----------------------------

This example showed how to work with a user with atuhentication and how to login to a website using sessions, flash messages through EJS views.
