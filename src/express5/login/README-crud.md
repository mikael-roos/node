---
revision:
    "2025-02-23": "(C, mos) Walkthrough, enhanced and moved to express 5."
    "2023-02-06": "(B, mos) Updated with MJS and improved structure in README."
    "2021-11-31": "(A, mos) First version."
---
![done](.img/crud-view-all.png)

Express with Create, Read, Update, Delete (CRUD)
==============================

This example shows how to use Express to achieve a CRUD functionality for a frontend consisting of web pages. The web pages are rendered on the server side using the template language EJS. 

The code structure is split into routes that has controlelr actions that uses a model that connects to a database.

You will practice how to CRUD flow for a collection in the database, where the frontend is web pages in a browser.

[[_TOC_]]

<!--
TODO

* Add solutions to the "own work"
* Add CSRF middleware, possible or use in a separate session on the basic protection mechanisms?

-->

<!--
Video
-----------------------------

This is a recorded presentation, 12 minutes long (English), when Mikael goes through the content of this article.

[![2023-02-07 en](https://img.youtube.com/vi/NKz5glWqaTc/0.jpg)](https://www.youtube.com/watch?v=NKz5glWqaTc)
-->



Preconditions
-----------------------------

You have worked through the [README.md introduction](./README.md) and you know how to start the example program.



Start the server
-----------------------------

Start the server.

```
npm run dev
```

Open a web browser and connect to it using: `http://127.0.0.1:3000/`.

Use the example through this start route:

* `/crud/users`



About CRUD in this example
-----------------------------

This example works towards the database table storing users. The example is built as a user interface for the administrator that wants to see all users, see details on a particular user, add a new user, update details on a user and delete a user. This is commonly known as Create, Read, Update, Delete (CRUD).

Lets walk through the example, step by step.

Ensure that you have the example code close to you, so you can review it in detail.

The main code to review is this.

| What | Source | Description |
|------|--------|-------------|
| Route | [`src/route/crudRoute.js`](./src/route/crudRoute.js) | The routes |
| Controller | [`src/controller/UsersController.js`](./src/controller/UsersController.js) | The controller actions for the routes |
| Model | [`src/model/UsersModel.js`](./src/model/UsersModel.js) | The user model that connects to the database table |

Lets do a walkthrough of the example application to see how it works.



View all `crud/users`
-----------------------------

The first thing is to present all users in a table. The controller asks the model to get the full collection from the database and it then sends it to the view which draws the collection into a webpage.

![view all](.img/crud-view-all.png)

_Figure. Show all the users in the collection._

The router, controller and model is doing the "same work" that you are used to. You have seen equal work before. The difference is how the controller prepares the `data` to be sent to the view and then renders it.

This is the controller action.

```js
  /**
   * Show all users by fetching data from the model and render the response.
   *
   * @param req
   * @param res
   * @param next
   * @async
   */
  async getAllUsers (req, res, next) {
    const data = {
      'users': await usersModel.getAllUsers()
    }
    res.render('users/view_all', data)
  }
```

The controller asks the model for the data and then renders it onto the view.

The view is a mix of HTML and EJS. The ejs code is javascript code within template tags.

The template file looks like this.

```html
<%- include('header') -%>

<h2>View all users</h2>

<p>Here are all the users in the database.</p>

<%- include('users_table') -%>
```

With ejs we can split the template files and include the bits and pieces that makes up the resulting page.

The ejs tag `<%- %>` renders the raw content without editing its content.

In this example it is the `users_table.ejs` that creates the html code for the table, based on the users in the collection.

```html
<table class="stripe">
  <thead>
    <tr>
      <th>User id</th>
      <th>Username</th>
      <th>Email</th>
      <th>Actions</th>
    </tr>
  </thead>
  <tbody>
    <% if (users?.length) { %>
    <% for (const user of users) { %>
    <tr>
      <td>
        <a href="./crud/users/<%= user.id %>"><%= user.id %></a>
      </td>
      <td>
        <%= user.username %>
      </td>
      <td>
        <%= user.email %>
      </td>
      <td>
        <a href="./crud/users/<%= user.id %>/update">Update</a>
        <a href="./crud/users/<%= user.id %>/delete">Delete</a>
      </td>
    </tr>
    <% } %>
    <% } else { %>
    <tr>
      <td colspan="3">
        There are no tasks to display
      </td>
    </tr>
    <% } %>
  </tbody>
</table>
```

Try to review the parts that are ejs constructs. You can find an if-statement, a loop and how to print out the variables.

You can see the if-statement that checks if the collection has any entries.

```html
<% if (users?.length) { %>

<% } else { %>

<% } %>
```

If it does, it loops through them.

```html
<% for (const user of users) { %>

<% } %>
```

The ejs tag `<% %>` allows to execute pure javascript code within the template.

Finally the content of each user is written to the html page.

```html
<a href="./crud/users/<%= user.id %>"><%= user.id %></a>
<%= user.username %>
<%= user.email %>
<a href="./crud/users/<%= user.id %>/update">Update</a>
<a href="./crud/users/<%= user.id %>/delete">Delete</a>
```

The ejs tag `<%= %>` escapes the content of the variable, before printing it. Escaping the value protects the page from XSS injections. An XSS injection could take place if the varaible contained javascript code.

This was the basics on how to read data from the database and present to the user in a html page, being rendered on the server, using views with the ejs template language.



View by id `crud/users/:id`
-----------------------------

This is how it can look like when the user clicked on the link of the user id in the table. It is then presented with a page that shows only the details of a single user.

Look at the url in the browser window, it says `crud/users/1` where 1 is the id of the user alice.

![view](.img/crud-view.png)

_Figure. View the details of a single user._

The controller can access the user id in `req.userId`, that was sent as a parameter in the request `crud/users/1`.

The controller asks the model to get all details of the user with the user id 1.

```js
/**
 * Get details on one user.
 *
 * @param req
 * @param res
 * @param next
 * @async
 */
async getUser (req, res, next) {
  const data = {
    'user': await usersModel.getUserById(req.userId)
  }
  res.render('users/view', data)
}
```

It stores the details into the data object that is sent to the view to be rendered.

The template file looks like this.

```html
<%- include('header') -%>

<h2>View user '<%= user.username %>'</h2>

<p>Here are details on the user.</p>

<table class="bgcolor">
  <tr>
    <th>User id</th>
    <td><%= user.id %></td>
  </tr>
  <tr>
    <th>Username</th>
    <td><%= user.username %></td>
  </tr>
  <tr>
    <th>User email</th>
    <td><%= user.email %></td>
  </tr>
</table>

<nav>
  <a href="./crud/users/<%= user.id %>/update">Update</a> |
  <a href="./crud/users/<%= user.id %>/delete">Delete</a>
</nav>
```

You can see in the template file how the ejs elements are integrated with the html page to provide the resulting web page that is then sent to the client.



Create new user `GET crud/users/create`
-----------------------------

There are two routes involved in creating a new user, one GET and one POST.

```js
// C to create a new user in the database
router.get('/users/create', controller.createUser)
router.post('/users/create', controller.createUserPost)
```

To create a new user we visit the route `GET crud/users/create` where we are presented with a html form where we enter details on the new user.

![view](.img/crud-create.png)

_Figure. A html form where we can create a new user._

The controller action that creates this response looks like this.

```js
/**
 * Present a form to create a new user.
 *
 * @async
 * @param req
 * @param res
 * @param next
 */
async createUser (req, res, next) {
  res.render('users/create')
}
```

The only thing that happens is that it renders a view through the template file `users/create.ejs`.

Looking at the template file we see almost only pure html and very little ejs.

```html
<%- include('header') -%>

<h2>Create a new user</h2>

<form method="post">
    <p>Username:<br><input type="text" name="username" value=""></p>
    <p>Email:<br><input type="text" name="email" value=""></p>
    <p>Password:<br><input type="password" name="password" value=""></p>
    <p>Password (again):<br><input type="password" name="passwordAgain" value=""></p>
    <p><input type="submit" name="doit" value="Create"></p>
</form>
```

Now we press the submit button.


Create new user `POST crud/users/create`
-----------------------------

When the form is submitted it is handled by the route `POST crud/users/create` where the controller talks to the model to create the user into the database collection.

```js
/**
 * Handle submitted form to create a new user to the database.
 *
 * @async
 * @param req
 * @param res
 * @param next
 * @param {string} name - The name of the user.
 * @param {string} email - The email of the user.
 * @param {string} password - The password for the user.
 */
async createUserPost (req, res, next) {
  const username = req.body.username
  const email = req.body.email
  const password = req.body.password
  const id = await usersModel.addUser(username, password, email)

  req.session.flashMessage = `User with id: ${id} created.` 
  res.redirect(`./${id}`)
}
```

The details from the submitted form is extracted from `req.body` and then sent to the model.

We use the flash message to create some kind of feedback to the user.

Then we redirect to the result page using `res.redirect(url)`. Remember the PRG design pattern.

The following redirect is relative the current page. The current page being `crud/users/create` so the resulting redirect url will then be `crud/users/:id` so it should land in the page showing details on the newly created user.

```js
res.redirect(`./${id}`)
```

Here it lands, after the redirect.

![create post](.img/crud-create-post.png)

_Figure. The flash message shows that the user was created and its now displayed in the form._

This is the basics on how to build up the flow in an application like this. Lets continue to see similair examples to fulfill how the complete CRUD application is developed.



Update `crud/users/:id/update`
-----------------------------

The update of the user is implemented similair to the create. It consits of two routes, one GET that presents the form and one POST that takes care of the submitted result.

```js
// U update details of the user
router.get('/users/:id/update', controller.updateUser)
router.post('/users/:id/update', controller.updateUserPost)
```

The GET route presents a form to show details of the user. Some of the details can be updated.

The controller action for this looks like this.

```js
/**
 * Present a form to update an existing user in the database.
 *
 * @async
 * @param req
 * @param res
 * @param next
 */
async updateUser (req, res, next) {
  const data = {
    'user': await usersModel.getUserById(req.userId)
  }
  res.render('users/update', data)
}
```

The resulting page can look like this.

![update](.img/crud-update.png)

_Figure. The user details are presented in a form and can be updated._

In this example we have opted so it is only the email that can be updated.

Pressing the submit button will post the form to the controller action looking like this.

```js
/**
  * Process submitted form and update user in database.
  *
  * @async
  * @param req
  * @param res
  * @param next
  * @param {string} email - The new email of the user.
  */
async updateUserPost (req, res, next) {
  const email = req.body.email
  const success = await usersModel.updateUser(req.userId, email)
  if (success) {
    req.session.flashMessage = `User with id: ${req.userId} was updated.` 
    res.redirect('.')
  } else {
    throw new Error('User not found')
  }
}
```

The incoming details from the submitted form is available in the `req.body`. The model is asked to update the details of the user.

The flash message is used to present the user with some feedback.

The redirect is done to the same page, so the user can continue to edit the details.

This is how it looks like when the form is submitted.

![update post](.img/crud-update-post.png)

_Figure. The user details are updated and the flash message provides feedback to the user._

That was how the update was implemented. Perhaps you can regcognize the same flow as when the user was created.



Delete `crud/users/:id/delete`
-----------------------------

The implementation of delete is similair to the implementation of create and update. First we have the two routes.

```js
// D delete users from database
router.get('/users/:id/delete', controller.deleteUser)
router.post('/users/:id/delete', controller.deleteUserPost)
```

Trying to delte a user will first show us some details from the user to be deleted.

![delete](.img/crud-delete.png)

_Figure. Show details of the user to be deleted._

Press the submit button and the user will be deleted.

![delete post](.img/crud-delete.png)

_Figure. The user was now deleted and the flash message provides feedback._

The redirect was made to the page where we could see all the users.

Lets quickly glance through hoe this was implemented in the controller.

First presentig the form with the details of the user to be deleted.

```js
/**
 * Present a form to delete a user from the database.
 *
 * @async
 * @param req
 * @param res
 * @param next
 */
async deleteUser (req, res, next) {
  const data = {
    'user': await usersModel.getUserById(req.userId)
  }
  res.render('users/delete', data)
}
```

Then taking care of the submitted form to actually delete the user and then redirect to the result page.

```js
/**
 * Delete a user from the database.
 *
 * @async
 * @param req
 * @param res
 * @param next
 */
async deleteUserPost (req, res, next) {
  const success = await usersModel.deleteUser(req.userId)
  if (success) {
    req.session.flashMessage = `User ${req.userId} deleted.` 
    res.redirect('..')
  } else {
    throw new Error('User not found')
  }
}
```

I guess that you can see the similarities between how the create, update and delete are implemented?



Search `crud/users/search`
-----------------------------

To finalize this example we shall look at how to implement a search. This is implemented through a self submitting form. That is a form that submits the result to the same route.

The route looks like this and it is a GET route.

```js
// Search users in the database
router.get('/users/search', controller.searchUser)
```

The first time a user access that route the resulting page looks like this.

![search](.img/crud-search.png)

_Figure. The search form is presented._

You can then enter details to search for. If you search for "li" you could get the following response.

![search post](.img/crud-search-post.png)

_Figure. The search form also presents the search from the results._

This is implemented in the follwoing controller.

```js
/**
 * Search for a user in the database.
 *
 * @async
 * @param req
 * @param res
 * @param next
 */
async searchUser (req, res, next) {
  const search = req.query?.search 
  let users 

  if (search) {
    users = await usersModel.searchUser(search)
  }

  const data = {
    search,
    users
  }
  res.render('users/search', data)
}
```

The controller reads the submitted details from the search form through the query string `req.query`.

In the second image above you could see that the form was submitted as a GET form and the form details are then sent though the url. The query string looks like this.

```
?search=li&doit=Search
```

You can format the query string to easier review its content.

```
?
  search=li
  &
  doit=Search
```

It is a list of key=value, separated by the `&`.

If the url contains a search query, then the model does it work with the database. The result is presented in a table.

This is the view that presents the search form and search results.

```html
<%- include('header') -%>

<h2>Search</h2>

<form>
    <p>
        Search:
        <br>
        <input type="search" name="search" value="<%= search ?? '' %>">
        <input type="submit" name="doit" value="Search">
    </p>
</form>

<% if (users?.length) { %>
<%- include('users_table') -%>
<% } %>
```

It is a standard GET form and at the end, if the search results contained some users, then it renderes those users with the `users_table.ejs` template file.



Review the implementation
-----------------------------

You know have a rough overview on how to implement a CRUD using web pages in express together with server side rendered web pages using the ejs template language.



### Own work, enhance the example

Choose some of the following features to enhance the example and "make it your own".

* [ ] When creating a new user, verify that the two passwords match, before creating the new user and supply a flash message saying if it did not match.
* [ ] When creating a user, and the passwords did not match, remember what the user entered in the username and the email form fields.
* [ ] Add a route where you can change the password of a user.



Summary
-----------------------------

This example showed how to work with CRUD using Express with a HTML page frontend that were rendered server side using the EJS template language.
