---
revision: 
    "2025-03-02": "(A, mos) First version."
---
<!--
![win](.img/win.png)
-->

Run the application through docker
==============================

This exercise shows how you can run this application through docker by using docker images that starts up as containers through the use of the command `docker compose` and its configuration file `docker-compose.yaml`.

You will also learn the basics on how to pull an image and run it as a container using the `docker` command.

[[_TOC_]]

<!--
TODO

* This exercise has a updated and self-contained exercise at the linux repo.
* There might be errors in this exercise in how it connects to the database.

-->

<!--
Video
-----------------------------

This is a recorded presentation, 12 minutes long (English), when Mikael goes through the content of this article.

[![2023-02-13 en](https://img.youtube.com/vi/tELvRoiho9I/0.jpg)](https://www.youtube.com/watch?v=tELvRoiho9I)
-->



Preconditions
-----------------------------

You need to install docker on you machine, you do this preferrably by installing [Docker Desktop](https://docs.docker.com/desktop/). 



Verify that docker works
-----------------------------

First we verifies that docker works on your terminal.

Execute the following command to start (and pull) a docker image from the docker hub that hosts standard images.

```bash
docker run --rm -it node bash
```

It pulls the image "node" from docker hub. You can check out the [node image on docker hub](https://hub.docker.com/_/node).

It then starts a container with it. The container is interactive `-it` and it starts a bash shell.

It can look like this.

```bash
$ docker run --rm -it node bash
root@59ebc940a0bb:/# node --version
v20.3.1
```

The version may vary depending what version of the image that you have locally.

It seems I had an older version so I decide to pull the latest one.

```bash
docker pull node
```

Then I start a container with that image again.

```bash
$ docker run --rm -it node bash
root@8076b6ac46f9:/# node --version
v23.9.0
```

Ah, that was a later version, at least when this article was written.

The option `--rm` ensures that the container is removed after its use. Without that option the container will reside on your computer and can be used later.



docker start, ps, exec and stop
-----------------------------

If you want to work with the exact same container, on repeated occasions, then you can start it in detached `-d` mode so it runs in the background.

```bash
$ docker run -d -it --name my-node node bash
d8f9ba7c35b4e0948152e3bc12789e97753bab853d9ded7f764e77e3f10114b5
```

The hash being displayed shows the id for the container. You can use that id, or the container name `--name my-node` to "talk" to the container.

You can now see the container using `docker ps`.

```bash
$ docker ps
CONTAINER ID   IMAGE            COMMAND                  CREATED          STATUS          PORTS                                             NAMES
d8f9ba7c35b4   node             "docker-entrypoint.s…"   3 seconds ago    Up 3 seconds                                                      my-node
```

The container seems to run. You can connect to it, using the id or the name.

```bash
$ docker exec -it my-node bash
root@d8f9ba7c35b4:/# 
```

Here is when using the first part of the id.

```bash
$ docker exec -it d8f9ba7c35b4 bash
root@d8f9ba7c35b4:/# 
```

You can stop the container.

```bash
$ docker stop my-node
my-node
```

Then verify its status using `docker ps -a`.

```bash
$ docker ps -a | grep my-node
d8f9ba7c35b4   node                                "docker-entrypoint.s…"   9 minutes ago    Exited (137) About a minute ago                                                     my-node
```

You can then restart it again and verify that it is running.

```bash
$ docker start my-node
my-node

$ docker ps
CONTAINER ID   IMAGE            COMMAND                  CREATED          STATUS          PORTS                                             NAMES
d8f9ba7c35b4   node             "docker-entrypoint.s…"   10 minutes ago   Up 6 seconds                                                      my-node
```

Try to connect to the container, to ensure all is working as is should.

```bash
$ docker exec -it my-node bash
root@d8f9ba7c35b4:/# 
```

That is (some of) the basic commands on how to use docker.



Build your own image using Dockerfile
-----------------------------
You can build your own image from a Dockerfile. Here is an example of an Dockerfile that builds a node server.

```dockerfile
# Use the official Node.js image from the Docker Hub
FROM node:latest

# Create and set the working directory
WORKDIR /app

# Copy package.json and package-lock.json
COPY package*.json ./

# Install app dependencies
RUN npm install

# Bundle app source
COPY . .

# Change to an ordinary user
USER node

# Start the server
CMD ["npm", "start"]
```

A Dockerfile contains instructions on how to create the image. An image is usually built upon another image, think of it as layers on layers. An image has some instructions and then you add another layer of instructions on top of it.

The file `.dockerignore` states what files not to be copied into the image when using the `COPY . . ` command.

Here is an example of the `.dockerignore` file for this example.

```
.git
node_modules/
package-lock.json
```

For this example we will use the command `docker compose` to build the image.


Tool `docker compose` and `docker-compose.yaml`
-----------------------------

The tool docker compose is a high level tool on top of docker. It uses a configuration file `docker-compose.yaml` to make it easy to configure several docker containers working together.

In this example we have two containers, one for the node server and one for the database. The `docker-compose.yaml` looks like this.

```yaml
volumes:
    mariadb_data: {}

services:

  server:
    build:
      context: .docker/server
      dockerfile: Dockerfile
    ports:
      - "3003:3000"
    volumes:
      - .:/app
      - /app/node_modules/
    depends_on:
      - mariadb

  mariadb:
    image: mariadb:latest
    restart: always
    ports:
      - "3308:3306"
    environment:
        MARIADB_ROOT_PASSWORD: pass
    volumes:
        - mariadb_data:/var/lib/mysql
        - .docker/mariadb/sql.d:/docker-entrypoint-initdb.d
        - .docker/mariadb/my.cnf:/root/.my.cnf
```

You see two services, one `server` and one `mariadb`.

The mariadb service is used from the [docker hub image called "mariadb:latest"](https://hub.docker.com/_/mariadb). This service uses a volume `mariadb_data` to store the changes to the database persistently.

The server service is built using the Dockerfile we saw previously.

Lets skip the details and instead we build the contianers and try to run them.



Pull and build with docker compose
-----------------------------

To make a fresh install we start by pulling the latest version of the mariadb database.

```bash
$ docker compose pull
[+] Pulling 10/10
 ✔ server Skipped - No image to be pulled         0.0s 
 ✔ mariadb Pulled                                10.8s 
   ✔ 5a7813e071bf Already exists                  0.0s 
   ✔ bdecd990c29c Pull complete                   0.4s 
   ✔ 5db80086e4da Pull complete                   1.1s 
   ✔ 901fe9394c00 Pull complete                   1.1s 
   ✔ 43eb19e1b102 Pull complete                   1.1s 
   ✔ 597f7afe50fe Pull complete                   9.2s 
   ✔ e1dede558384 Pull complete                   9.2s 
   ✔ 5c3a22df929b Pull complete                   9.2s 
```

The database image is pulled, layer by layer.

You can see that the server image is not pulled, that is to be built locally. Lets do that.

```bash
$ docker compose build server
Compose now can delegate build to bake for better performances
Just set COMPOSE_BAKE=true
[+] Building 8.7s (11/11) FINISHED                         docker:default
 => [server internal] load build definition from Dockerfile          0.0s
 => => transferring dockerfile: 382B                                 0.0s
 => [server internal] load metadata for docker.io/library/node:latest 0.0s
 => [server internal] load .dockerignore                             0.0s
 => => transferring context: 77B                                     0.0s
 => [server 1/5] FROM docker.io/library/node:latest                  0.0s
 => [server internal] load build context                             0.0s
 => => transferring context: 17.26kB                                 0.0s
 => CACHED [server 2/5] WORKDIR /app                                 0.0s
 => [server 3/5] COPY package*.json ./                               0.0s
 => [server 4/5] RUN npm install                                     8.0s
 => [server 5/5] COPY . .                                            0.0s 
 => [server] exporting to image                                      0.6s 
 => => exporting layers                                              0.6s 
 => => writing image sha256:2a46012b89456faa4a13935f1e426223134bae47 0.0s 
 => => naming to docker.io/library/login-server                      0.0s 
 => [server] resolving provenance for metadata file                  0.0s 
[+] Building 1/1
 ✔ server  Built                                                     0.0s 
 ```

Now the images are built and we can start them.



Pull and build with docker compose
-----------------------------

We start both containers at once, in the detached mode.

```bash
$ docker compose up -d
[+] Running 4/4
 ✔ Network login_default        Created       0.0s 
 ✔ Volume "login_mariadb_data"  Created       0.0s 
 ✔ Container login-mariadb-1    Started       0.6s 
 ✔ Container login-server-1     Started       0.7s 
```

We can see that both containers execute in the background using `docker composer ps`.

```bash
$ docker composer ps
NAME              IMAGE            COMMAND                  SERVICE   CREATED          STATUS          PORTS
login-mariadb-1   mariadb:latest   "docker-entrypoint.s…"   mariadb   40 seconds ago   Up 38 seconds   0.0.0.0:3308->3306/tcp, [::]:3308->3306/tcp
login-server-1    login-server     "docker-entrypoint.s…"   server    40 seconds ago   Up 38 seconds   0.0.0.0:3003->3000/tcp, [::]:3003->3000/tcp
```

We can attach to any of the containers.

First we try to attach to the mariadb container and execute the terminal client on it.

```bash
$ docker compose exec mariadb mariadb maria
Reading table information for completion of table and column names
You can turn off this feature to get a quicker startup with -A

Welcome to the MariaDB monitor.  Commands end with ; or \g.
Your MariaDB connection id is 3
Server version: 11.7.2-MariaDB-ubu2404 mariadb.org binary distribution

Copyright (c) 2000, 2018, Oracle, MariaDB Corporation Ab and others.

Type 'help;' or '\h' for help. Type '\c' to clear the current input statement.

MariaDB [maria]> 
```

Then we attach to the server container and checking what version of node it has.

```bash
$ docker compose exec server bash
node@3c4bd14f7759:/app$ node --version
v23.9.0
```

By looking in the file `docker-compose.yaml` we see that the server runs on the port 3003 so we can open a web browser to that url to verify that the application works like it expects.



Start and stop the containers
-----------------------------

We can stop and start the containers.

```bash
$ docker compose stop
[+] Stopping 2/2
 ✔ Container login-server-1   Stopped     0.7s 
 ✔ Container login-mariadb-1  Stopped     0.4s 
 ```

We verify that no containers are running.

```bash
$ docker compose ps
NAME      IMAGE     COMMAND   SERVICE   CREATED   STATUS    PORTS
```

Then we start them again.

```bash
$ docker compose start
[+] Running 2/2
 ✔ Container login-mariadb-1  Started       0.1s 
 ✔ Container login-server-1   Started       0.2s 
```



Take the containers down
-----------------------------

You can also take the containers down completely. That is the opposite to `up`.

When you take the containers down, you can also remove the volumes connected to them.

```bash
$ docker compose down --volumes
[+] Running 4/4
 ✔ Container login-server-1   Removed          0.8s 
 ✔ Container login-mariadb-1  Removed          0.4s 
 ✔ Volume login_mariadb_data  Removed          0.0s 
 ✔ Network login_default      Removed          0.2s 
 ```

This is a way to clean up and restart fresh containers.



Where to go from here
-----------------------------

Before you leave, check the Dockerfile and the `docker-compose.yaml`once more so you are certain of the bits and pieces in them. It is these files that you need to create on your own, the next time you want to build images and run containers.



Summary
-----------------------------

This example showed how to work with docker images, containers using `docker` and `docker compose`.
