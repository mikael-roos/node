---
revision: 
    "2025-02-21": "(C, mos) Walkthrough, enhanced and moved to express 5."
    "2023-02-06": "(B, mos) Updated with MJS and improved structure in README."
    "2021-11-31": "(A, mos) First version."
---
![win](.img/win.png)

Express with session and flash messages
==============================

This example shows how to use Express with sessions. The example shows how one can implement a small guessing game using the session and how to work with flash messages to send notices to the next page rendering.

The example uses views with the template language EJS.

[[_TOC_]]

<!--
TODO

* Add section on XSS and explain how EJS deals with it.
* Add solutions to the own work below.

-->

<!--
Video
-----------------------------

This is a recorded presentation, 12 minutes long (English), when Mikael goes through the content of this article.

[![2023-02-13 en](https://img.youtube.com/vi/tELvRoiho9I/0.jpg)](https://www.youtube.com/watch?v=tELvRoiho9I)
-->



Preconditions
-----------------------------

You have worked through the [README.md introduction](./README.md) and you know how to start the example program.



Start the server
-----------------------------

Start the server.

```
npm run dev
```

Open a web browser and connect to it using: `http://127.0.0.1:3000/`.

Use the example through this start route:

* `/guess/`



Add support for session to the server
-----------------------------

To enable the session we use a [middleware express-session](http://expressjs.com/en/resources/middleware/session.html). This is already installed as part of the `package.json` and it is enabled in the `src/express.js` server like this.

```js
import session from 'express-session'
import { sessionOptions } from './config/sessionOptions.js'

// Enable session
app.use(session(sessionOptions))
```

The options to configure the session is in its own file. You can review the content of that file through [`src/config/sessionOptions.js`](./src/config/sessionOptions.js).

The configuration uses environment variables to configure parts of the session. These are added in the `.env` file.

```bash
# This is the name of the session cookie that will be set in the user's browser.
SESSION_NAME='session-name'

# This is a secret key used to sign the session ID cookie.
# Signing the cookie ensures that the session ID hasn't been tampered with.
SESSION_SECRET='should be a secret here'
```

When the session works, all its data will be saved into the session variable `req.session`.

The session variable will be undefined if you forget to add the session into express.

All parts of the `req.session` will be available in the templates when rendering using ejs.



The game
-----------------------------

To show off how the session works, a small guessing game is implemented where the user should try to guess on a number that the server thinks on, between 1 and 100.

The game is implemented through these routes.

```javascript
router.get('/', controller.home)
router.post('/init', controller.init)

router.get('/guess', controller.guess)
router.post('/guess', controller.guessCheck)

router.get('/cheat', controller.cheat)
```

The actual session data is available as `req.session` and you can save your own data to it, for example using `req.session.data = 'some data'`.

All data in `req.session` will persist until the next request.

Lets review how the game looks while playing it.



### Start the game

First you need to start the game. This will also initiate the session and randomize a number and store it in the session. The number of guesses are now set to 0.

![start](.img/home.png)

_Figure. The home page of the guessing game._



### Guess a number

You can then guess on a number.

![guess](.img/guess.png)

_Figure. The start of the game where you can guess the number._

The route will check if the guess is correct or not and it will save a message in the session that is printed on the result page. This is known as a "flash message" and it is a way to send a message to the next route.

You will get a hint, if your guess is wrong.

![win](.img/too-high.png)

_Figure. The guess was wrong and you get a hint on how to guess the next number._

If you guess the correct number you win the game.

![win](.img/win.png)

_Figure. The guess was correct and you won the game._



### Session

You can debug the session and see what it contains.

![session](.img/session.png)

_Figure. The session contains the needed details to implement the game._



The template files in `src/views`
-----------------------------

Each view is rendered by the controller, for example the home view.

```js
controller.home = (req, res) => {
  res.render('guess/home')
}
```

The view files, or template files, are available in the `src/views` directory. It is good practice to organise the view files in a directory structure.

Each view file has the extension `.ejs`. The view file for `guess/home` is then available in [`src/views/guess/home.ejs`](./src/views/guess/home.ejs).

You can send data to be rendered into the view. It can look like this.

```js
controller.guess = (req, res) => {
  const lastGuess = req.session.lastGuess ?? null
  const numGuesses = req.session.numGuesses ?? 0

  req.session.lastGuess = null

  const data = {
    lastGuess,
    numGuesses
  }
  res.render('guess/guess_form', data)
}
```

The `data` object is populated with details that is sent to the view. The view file can print the details as part of the rendered view.

```html
<%- include('header') %>

<h2>Guess</h2>

<p>Guess on a number between 1 and 100.</p>

<form method="post">
    <p>
        <input type="number" name="guessedNumber" value="<%= lastGuess %>">
    </p>
    <p>
        <input type="submit" value="Guess">
    </p>
</form>

<p>You have made <%= numGuesses ?? 0 %> guesses.</p>
```

The template tags `<%- %>` and `<%= %>` decide how the content will be rendered to the page.



The flash messages
-----------------------------

The flash messages are a way to use the session to send feedback messages to the user. The flash messages are saved in the sessions, by the server. For example like this.

```js
  req.session.numGuesses = numGuess + 1
  req.session.lastGuess = lastGuess
```

On the next request the is a middleware `flashMessage()` that performs a read-once on the flash message, if it exists.

```js
/**
 * Save the read once flash messages to res.locals to be used in the views and
 * then remove it from the session.
 *
 * @param {object} req Express request object.
 * @param {object} res Express response object.
 * @param {object} next Express next object.
 */
middleware.flashMessage = (req, res, next) => {
  res.locals.flashMessage = req.session.flashMessage ?? null
  req.session.flashMessage = null

  next()
}
```

The middleware stores the flash message to `res.locals.flashMessage`. This means the `flashMessage` will be available for the template file when the view is rendered.

A template file, to render the actional flash message, can then look like the `src/views/partials/flash.ejs`.

```html
<% if (locals.flashMessage) { %>
    <p style="background-color:aqua; padding: 0.5em"><%- flashMessage %></p>
<% } %>
```



The layout of the pages
-----------------------------

The example uses npm package `express-ejs-layouts` to enhance the template language EJS with support for layouts.

A layout is a template for the HTML page to render.

```html
<!doctype html>
<html lang="en">

<head>
  <meta charset="utf-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1" />
  <base href="<%= baseURL %>" />
  <title>Website with EJS templates</title>
  <link rel="stylesheet" href="css/style.css" />
</head>

<body>
  <%- include('../partials/header') %>
  <main>
    <%- include('../partials/flash') %>
    <%- body %>
  </main>
  <%- include('../partials/footer') %>
</body>

</html>
```

The part of the actual view file rendered, is rendered into the area of `<%- body %>`. 

The rest is the template, or layout, added to the rendered page.



Redirect
-----------------------------

The example is implemented with the common design pattern called "[Post/Redirect/Get (PRG)](https://en.wikipedia.org/wiki/Post/Redirect/Get)". That is to make sure that a submitted form is only posted once.

This is how PRG Works:

1. POST: The user submits a form (via an HTTP POST request).

2. REDIRECT: The server processes the form data and responds with an HTTP redirect (HTTP status code 302 or 303) to a new URL.

3. GET: The user's browser follows the redirect and makes a GET request to the new URL, which displays the result or a confirmation page.

Here is an illustration on how this works.

![prg pattern](.img/prg-pattern.png)

_Figure. An activity diagram of the Post/Redirect/Get (PRG) design pattern._

We can see in the routes and the controller how this is implemented.

First we have the routes for guessing a number.

```js
router.get('/guess', controller.guess)
router.post('/guess', controller.guessCheck)
```

The controller action `controller.guess` presents. or renders, the HTML form, where a guess can be made.

```js
/**
 * Let the user make a guess.
 */
controller.guess = (req, res) => {

  // Prepare the data object and rendet the view

  const data = {
    lastGuess,
    numGuesses
  }
  res.render('guess/guess_form', data)
}
```

The form is then submitted as POST and the controller action `controller.guessCheck` deals with it.

```js
/**
 * Process the guess made by the user.
 */
controller.guessCheck = (req, res) => {

  // Deal with the submitted form, then redirect to the presenting page

  res.redirect('./guess')
}
```

As a final step the controller action makes a GET redirect to the presentation page. 



Review the implementation of the game
-----------------------------

The actual code of this game can be found through the routes [`src/route/guessRoute.js`](./src/route/guessRoute.js) and the controller [`src/controller/guessController.js`](./src/controller/guessController.js).



### Considerations and flaws with the implementation

The example has implemented the guessing game code within the controller. This is a small game, but it should really be implemented into a model game class to follow the rule of "thin controllers and fat models".

Perhaps you would like to rewrite the game engine and move the code to a model class/module?

You could then also try to implement so the game enforces max 5 guesses and that you can not continue to make any guesses when you have won the game.

Perhaps you also should consider what happen when the user enters something that is not a number? Do you need to check that on the server side?

What happens when the user guess a number that is higher than 100 or lower than 1? How should you deal with that?

What happens when the user wins the game, should it restart somehow?



### Own work, enhance the game

Choose some of the following features to enhance the game and "make the game your own".

* [ ] Add error handling for when the user makes a guess > 100 or < 1, use flash messages.
* [ ] Add error handling if the user enters something else than a number.
* [ ] Add a check saying the the user has already tried to guess that number before.
* [ ] Limit the number of guesses to 5.
* [ ] Fix so the game can restart if the user wins or if it becomes game over.
* [ ] Enhance the flash messages to differ between info, warning and errors.
* [ ] Rewrite the game into a model `src/models/guessModel.js`.



Summary
-----------------------------

This example showed how to work with the session and flash messages.
