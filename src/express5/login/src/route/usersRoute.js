import express from 'express'
import controller from '../controller/UsersJsonController.js'

export const router = express.Router()

router.param('id', controller.verifyUserId)

router.get('/users', controller.getAllUsers)
router.post('/users', controller.addUser)
// router.get('/users/:id',  controller.getUserById)
router.put('/users/:id', controller.updateUser)
// router.patch('/users/:id',  controller.partialUpdateUser)
router.delete('/users/:id', controller.deleteUser)
// router.delete('/users',  controller.deleteUsers)
