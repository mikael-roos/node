---
revision: 
    "2025-02-19": "(A, mos) First version."
---
Exercise: How to start the example code
==============================

<!-- ![done](.img/done.png) -->

This explains how to start the example code.


[[_TOC_]]


<!--
TODO

* 
-->



<!--
Video
-----------------------------

This is a recorded presentation, 18 minutes long (English), when Mikael goes through the content of this article.

[![2023-02-13 en](https://img.youtube.com/vi/wM1RPbxamC4/0.jpg)](https://www.youtube.com/watch?v=wM1RPbxamC4)
-->



Preconditions
-----------------------------

This exercise contains code seen in the exercise "[Exercise: Build a JSON REST API server for CRUD a users collection with a MariaDB (MySQL) database](../database/mariadb/json-crud/)" and it therefores uses the database created in that example.



The example code
-----------------------------

This example includes an Express server to work with. You can start the server locally on your machine or using docker-compose.

The example also contains script that connects to the servern, these scripts are located in [`script/`](./script/).



Clone the repo
-----------------------------

You can clone the whole exercise repo like this.

```
git clone https://gitlab.com/mikael-roos/node.git
cd node
ls -al
```

This particular exercise lies in the directory `src/express5/jwt` and you need to move to that directory.

```
cd src/express5/jwt
ls -al
```



Start the server locally
-----------------------------

Install the dependencies:

```
npm install
```

Create your `.env` file by copying the `.env.example`.

```
cp .env.example .env
```

Review the file and edit its details if needed. It contain the essentials to connect to the database server so you need to adapt it to your local environment.

Start the server:

```
npm start
```

_If you get a database error, then review the next section below on how to "Init the database"._

Open a web browser and connect to it using: `http://127.0.0.1:3000/`.

You can optionally start the server on another port like this.

```
PORT=3001 npm start
```



Init the database
-----------------------------

If you for some reason need to initiate the database from scratch, then you can do so using the following scripts.

The script [`sql/create-user-maria.sql`](./sql/create-user-maria.sql) will create the database user that the examples use. You do not need to use this user, you can use another database user that you already have.

You need to be root user (or equal) to create a new database user.

```
mariadb -uroot -p < sql/create-user-maria.sql
```

The exampe program uses a database called `maria` for the examples. You can create that with the script [`sql/create-database.sql`](./sql/create-database.sql).

```
mariadb < sql/create-database.sql
```

This particular exercise uses a database table `user_jwt` that is created through the script [`sql/create-user-table.sql`](./sql/create-user-table.sql).

```
mariadb < sql/create-user-table.sql
```




<!--
Start the server using docker compose
-----------------------------

You can build the image and start the container like this.

```
# Build the server
docker compose build server

# In the foreground
docker compose up server

# In the background, detached
docker compose up -d server
```

Open a web browser and connect to it using: `http://127.0.0.1:3000/`.

You can stop the container using `ctrl-c` (foreground) or with `docker compose down` (background).

-->
