---
revision: 
    "2025-02-03": "(B, mos) Updates after lecture."
    "2025-02-02": "(A, mos) First version."
---
![header](.img/structure.png)

Exercise: Setup an express server for "Hello World"
==============================

This example shows how to get going with Express.js by adding routes and a development environment and by using a directory structure that prepares for larger applications.

[[_TOC_]]

<!--
TODO

* 

-->



<!--
Video
-----------------------------

This is a recorded presentation, 18 minutes long (English), when Mikael goes through the content of this article.

[![2023-02-13 en](https://img.youtube.com/vi/wM1RPbxamC4/0.jpg)](https://www.youtube.com/watch?v=wM1RPbxamC4)
-->



Install express
-----------------------------

The aim is to install Express and configure it as an application server that says "Hello World". While doing that we also need a proper development environment to work in.

You can read background information on this, in the documentation for express about "[installing express](https://expressjs.com/en/starter/installing.html)" and "[create a hello world for express](https://expressjs.com/en/starter/hello-world.html)".


Lets start by creating a directory structure for the project.

```
mkdir hello
cd hello
```

We then initiate this as a npm project.

```
npm init
```

Now we install express.

```
npm install express@5
```

You can read more on the [npm package express](https://www.npmjs.com/package/express) in the npm repository.



Configure the project
-----------------------------

We want to use ESM modules so we need to add the following line in `package.json`.

```json
"type": "module"
```

We also add a script that to make it easy to re-install.

```json
  "scripts": {
    "clean": "rm -rf node_modules/ package-log.json"
  },
```



Add the start script
-----------------------------

Add the `app.js` script which is the entry point to start the server.

```js
import { app } from './src/express.js'

const port = process.env.PORT || 3000

app.listen(port, () => {
  console.log(`Listening on port ${port}`)
})
```

Add the startup script for express in `src/express.js`.

```js
import express from 'express'

export const app = express()
```

Verify the server can start by running it.

```
node app
```

You should also add the startup part to the script part in `package.json`.

```
  "scripts": {
    "start": "node app",
  },
```



Create "Hello World" routes
-----------------------------

we shall add two routes to the express server, this is done in the `src/express.js`.

This is a route, replying with the JSON response "Hello World".

```js
app.use('/hello', (req, res) => {
  res.send('Hello World!')
})
```

This is a route, replying with the JSON response "Hello World".

```js
app.use('/api/hello', (req, res) => {
  const json = {
    "message": "Hello World!"
  }
  res.json(json)
})
```

Verify that the routes works by restarting the server and then access them using a web browser.



Move the routes to their own files
-----------------------------

To prepare for a larger application we create a directory where we store all routes.

Create the file `src/route/hello.js` and move the routes there.

It can look like this when you are done.

```js
import express from 'express'

export const router = express.Router()

router.get('/hello', (req, res) => {
  res.send('Hello World!')
})

router.get('/api/hello', (req, res) => {
  const json = {
    "message": "Hello World!"
  }
  res.json(json)
})
```

The routes needs to be imported into the server.

First, import the router into `src/route/index.js`.

```js
import express from 'express'
import { router as helloRoute } from './hello.js'

export const router = express.Router()

router.use('/', helloRoute)
```

Then add the router to the app server in `src/express.js`.

```js
import { router } from './route/index.js'

app.use('/', router)
```

Verify that the routes works as before.

We split the code into these files to make it easy to add more routes.



Add nodemon to avoid restarts
-----------------------------

Install the [package nodemon](https://www.npmjs.com/package/nodemon) that helps during development so you do not need to restart the server while doing changes to the source code.

```
npm install nodemon --save-dev
```

Start the server using nodemon.

```
npx nodemon app
```

Add a script to start it.

```json
  "scripts": {
    "dev": "npx nodemon app",
  },
```

Now try starting it using `npm run dev`. Do some changes in a source file, save it and what the server being reloaded.



Add logger morgan
-----------------------------

When working you will want to logg things that happens and for development you will want to have the possibility to visual see what happens in the server. The [logger morgan](https://www.npmjs.com/package/morgan) can help with that.

```
npm install morgan
```

Add it to the `src/express.js` so the application server is aware of it.

```js
import logger from 'morgan'

// Use the morgan logger
app.use(logger('dev', { immediate: true }))
```

Verify that the logger works by accessing the routes. You should see some logger messages.

A logger like this is called a "middleware".



Add dotenv for configuration
-----------------------------

We want to store our configuration in dotenv files and we want to have node/nodemon to read the configuration files when starting up.

The ability to read `.env` files is built in to the latest versions (>22) of node.

This is how to configure node and nodemon to read input from the `.env` file.

```json
  "scripts": {
    "start": "node --env-file=.env app",
    "dev": "npx nodemon --env-file=.env app",
  },
```

Create the dotenv file `.env` and add the port to it.

```
PORT=3001
```

Start the server and verify it starts on port 3001. Then change it back to port 3000 (or whatever port you want to use).



Add and error handler
-----------------------------

Lets add error handlers to the server `src/express.js`. We put this code into a separate file in the `src/middleware/` directory.

The code for the error handlers are put into the file `src/middleware/errorHandler.js`.

First we add the 404 error handler that will deal with requests to a non existing path.

```js
import http from 'http'

export const errorHandler = {}

/**
 * Default handler for 404 routes when the resource is not found.
 *
 * @param {object} req Express request object.
 * @param {object} res Express response object.
 * @param {function} next Express next function.
 */
errorHandler.notFoundDefault = (req, res, next) => {
  const err = new Error(http.STATUS_CODES[404] || 'Not Found')
  err.status = 404
  next(err) // Pass the error to the global error handler
}
```

Then we add a global error handler that shall catch all errors in the application.

```js
/**
 * Global error handler.
 *
 * @param {object} err The caught error.
 * @param {object} req Express request object.
 * @param {object} res Express response object.
 * @param {function} next Express next function.
 */
errorHandler.errorDefault = (err, req, res, next) => {
  console.error(err.stack) // Log the error stack for debugging

  const statusCode = err.status || 500
  const message =
    process.env.NODE_ENV === 'production'
      ? 'Something went wrong' // Hide error details in production
      : err.message

  res.status(statusCode).json({
    status: statusCode,
    message,
  })
}
```

Now we need to add the error handlers so that express knows about it. This is done in `src/express.js`.

```js
import { errorHandler } from './middleware/errorHandler.js';

// 404 handler
app.use(errorHandler.notFoundDefault);

// Global error handler
app.use(errorHandler.errorDefault);
```

Now try to access a route that does not exists, it should result in a 404.

> `{"status":404,"message":"Not Found"}`

Add the following route that throws an exception, to verify that the global error handler works when exceptions are thrown. You can add it into your `src/route/hello.js` router file.

```js
import http from 'http'

router.get('/500', (req, res, next) => {
  const err = new Error(http.STATUS_CODES[500] || 'Internal Server Error');
  err.status = 500;
  next(err); // Pass the error to the error handler
});
```

Try to access the route `/500` to see what happens. The response should be:

> `{"status":500,"message":"Internal Server Error"}`



Add a linter with eslint
-----------------------------

A linter helps maintaining a code standard and avoid mess in the code.

Install [the linter eslint](https://www.npmjs.com/package/eslint) as a development tool and configure it.

```
npm i eslint --save-dev
```

Add a configuration file `eslint.config.js` for it. Here is a default you can use.

```js
export default [
  {
    ignores: ["node_modules/**"],
  },
  {
    files: ["**/*.js"],
    languageOptions: {
      ecmaVersion: "latest",
      sourceType: "module",
    },
    rules: {
      "no-unused-vars": "warn",
      "no-console": "off",
      "semi": ["error", "never"],
      "indent": ["error", 2],
    },
  },
]
```

Lets add scripts to make it easy to run the linter and its fixer.

```json
  "scripts": {
    "eslint": "npx eslint .",
    "eslint:fix": "npx eslint . --fix"
  },
```

Run the linter to see if you have any errors and the try to fix them, first automatically and then you need to go into the code manually.

First run the linter.

```
npm run eslint
```

Then try to fix the issues automatically.

```
npm run eslint:fix
```

Continue to run the linter to assure you have a good coding standard and to avoid mess in your code.



Add a `public/` with a static website
-----------------------------

We can add static web resources to the app server, just like an ordinary web server with static content for html, css and js.

Configure this in the `src/express.js` file so express knows what directory to use.

```js
// Use the public folder for static resources
app.use(express.static('public'))
```

Create the directory `public/` and add a html file `index.html` to it, then put the following content into it, its a html page.

```html
<!doctype html>
<html>
<style>
body {
    max-width: 800px;
    margin: 0 auto;
}
h1 {
    border-bottom: 4px double #aaa;
}
h2 {
    border-bottom: 1px solid #ccc;
}
</style>

<body>
<h1>Hello World</h1>

<p>This exercise is about how to get going with Express.</p>


<h2>Verify that the server works</h2>

<p>Before you start you can verify that these routes works as expected from the server.</p>

<ul>
    <li><a href="hello"><code>/hello</code> - Say hello in a ordinary web page.</a></li>
    <li><a href="api/hello"><code>/api/hello</code> - Say hello using JSON response.</a></li>
    <li><a href="500"><code>/500</code> - Generate a 500 response.</a></li>
</ul>
</body>
</html>
```

Feel free to update the web page to use an external stylesheet and add a favicon to make it look nicer.



Write compatible scripts
-----------------------------

We want to be able to run the npm scripts in both Window and Linux environenments, we want the scripts to be compatible in many different terminals.

One way to do that is to install a helper tool [cross-env](https://www.npmjs.com/package/cross-env). Lets do that.

```
npm install --save-dev cross-env
```

We will use this small program in the next section when we add a script to execute the unit tests in a compatible way.



Add unit test
-----------------------------

We can add unit testing to the application. First we install the tools we use for unit testing.

* [Mocha](https://www.npmjs.com/package/mocha)
* [Chai](https://www.npmjs.com/package/chai)
* [SuperTest](https://www.npmjs.com/package/supertest)

Lets install them as dev dependencies.

```
npm i --save-dev mocha chai supertest
```

The test files we put into the directory `test/`.

We add a test file for the hello routes `test/route/hello.test.js` to start writing the unit tests.

This is a unit test that checks if the route path `/` returns a web page with the content "Hello World".

```js
import { app } from '../../src/express.js' 
import { expect } from 'chai';
import supertest from 'supertest'

const request = supertest(app)

describe('Express Hello Routes', () => {
  it('should return "Hello World" for GET /', async () => {
    const res = await request.get('/')
    expect(res.status).to.equal(200)
    expect(res.text).to.contain('Hello World')
  })
})
```

We add the script to run the tests into `package.json`. This script is written in a compatible way so the environment variable will set in both Linux and Windows terminals.

```json
  "scripts": {
    "test": "npx cross-env NODE_ENV=test npx mocha test/**/*.test.js",
  },
```

The part with `npx cross-env NODE_ENV=test` is to set an environemnt variable in a compatible way. This setting can be used to know when the test suite is executed or if the program is executing in a normal fashion.

Now we can run the tests.

```
npm test
```

We can add more test cases for the other routes we have.

This is a test that verifies that a non existing route produces a 404.

```js
  it('should return 404 for non-existent route', async () => {
    const res = await request.get('/not-found')
    expect(res.status).to.equal(404)
    expect(res.body).to.have.property('status')
    expect(res.body.status).to.equal(404)
  })
```

Continue to add test cases for all routes.

This type of tests can be considered functional tests or integration tests since they test all code related to a specific http request.



<!--
No logger during tests
-----------------------------

We can tell the logger morgan to not log during tests.

First exclude the logger in the environment is "test".

```js
if (process.env.NODE_ENV !== 'test') {
  app.use(morgan('dev'))
}
```

The set the environment to be test while running mocha.

```js
    "test": "NODE_ENV=test mocha test/**/*.test.js",
```
-->



Add a `.gitignore`
-----------------------------

Before we leave we add a `.gitignore` so we only checkin the files we want.

Create the file `.gitignore` and add the following to it.

```
node_modules/
.env

# Coverage
.nyc_output/
coverage/

# Mac-specific files
.DS_Store

# Editor directories and files
.idea
.vscode
*.swp
*~

# Temporary files
tmp/
temp/
```
