---
revision: 
    "2025-02-19": "(A, mos) First version."
---
Exercise: Work with webhooks
==============================

XXX




<!-- ![done](.img/done.png) -->

This is a multi-exercise where we use one example program to exemplify how you can work with express and sessions, how to do CRUD with HTML forms and how to implement authentication with login and logout using the session.

This example uses a web frontend where the web pages are dynamically rendered using the templete engine Embedded JavaScript (EJS). 

This document provides an overiew of the included exercises and how they relate to other exercises.

This document also explains how to setup and start the example code.

[[_TOC_]]


<!--
TODO

* Prepare for this with a lecture on the new issues.
* (Add XSS example)
* Add exercise login using 2Fa TOTP
* Add exercise with client side rendering (or make that its own exercise?)
-->



<!--
Video
-----------------------------

This is a recorded presentation, 18 minutes long (English), when Mikael goes through the content of this article.

[![2023-02-13 en](https://img.youtube.com/vi/wM1RPbxamC4/0.jpg)](https://www.youtube.com/watch?v=wM1RPbxamC4)
-->



Preconditions
-----------------------------

This exercise contains code seen in the exercise "[Exercise: Build a JSON REST API server for CRUD a users collection with a MariaDB (MySQL) database](../database/mariadb/json-crud/)" and it therefores uses the database created in that example.



Overview of the exercises
-----------------------------

This multi-exercise contains the following exercises.

| Document | Explanation |
|----------|-------------|
| [`README.md`](./README.md) | This document, provides an introduction to the exercises and shows how to start up the example program. |
| [`README-session.md`](./README-session.md) | Exercise on  how to integrate the session into the server and show how to use the session to implement a guessing game. |
| [`README-crud.md`](./README-crud.md) | Exercise on  how to work with HTML forms and CRUD to the database and a collection of users. |
| [`README-login.md`](./README-login.md) | Exercise on  how to login to a website using sessions and HTML forms. |



The example code
-----------------------------

This example includes an Express server to work with. You can start the server locally on your machine or using docker-compose.

<!--
The example also contains script that connects to the servern, these scripts are located in [`script/`](./script/).
-->



### Support HTML forms as POST

When a HTML form is posted to the server, using the `method="POST"`, the content of the form is submitted through the HTTP request body and usually encoded as `application/x-www-form-urlencoded`.

We need to let the express server know that data of that sort should be dealt with. This is how to configure that in `src/express.js`.

```js
// Parse requests of the content type application/x-www-form-urlencoded.
// Populates the request object with a body object (req.body).
app.use(express.urlencoded({ extended: false }))
```

When dealing with a submitted form the content from that submitted form will now be available as part of the `req.body`.



### EJS to render pages

This code example uses the [EJS template language](https://ejs.co/) to render dynamic pages onto a page layout. There are two npm packages that are used to accomplish this. The package [`ejs`](https://www.npmjs.com/package/ejs) is the template engine and [`express-ejs-layouts`](https://www.npmjs.com/package/express-ejs-layouts) enables to work with a page layout common for all pages.

The packages are installed in the [`package.json`](./package.json).

The following code is needed to add the features into the server in the file [`src/express.js`](./src/express.js).

```js
import expressLayouts from 'express-ejs-layouts'

// View engine setup.
app.set('view engine', 'ejs')
app.set('views', path.join('src', 'views'))
app.set('layout', path.join('layout', 'default'))
app.use(expressLayouts)
```

The views are all stored in the [`src/views`](./src/views/) directory.



### Using BASE_URL

This example uses the html element to set the base url for all links within the page. The layout for the pages looks like this.

```html
  <base href="<%= baseURL %>" />
```

This means that all relative url's till be prepended with the base url, making it easy to configure if the application is set into production in a sub directory.

The base url can be configured through the `.env` file as an environment variable, like this.

```bash
# Base url for the site, use '/' when the site is run directly in the webroot.
# Change if the site is run as a subdirectory, for example '/subdir/'.
# Defaults to '/'.
BASE_URL='/'
```

In the express server there is a middleware that extracts the value for the BASE_URL and injects it into the `res.locals` to make the value visible in tje `.ejs` view files.

The middleware looks like this.

```js
const middleware = {}
export { middleware as localsMiddleware }

/**
 * Extract details of the BASE_URL from the environment and inject it into 
 * response.locals to make it visible in the view files.
 *
 * @param {object} req Express request object.
 * @param {object} res Express response object.
 * @param {object} next Express next object.
 */
middleware.injectBaseUrl = (req, res, next) => {
  // Pass the base URL to the views.
  res.locals.baseURL = process.env.BASE_URL || '/'

  next()
}
```

The middleware is added to the server configuration in the `src/express.js` like this.

```js
import { localsMiddleware } from './middleware/injectLocals.js'

// Middleware to inject into response.locals
app.use(localsMiddleware.injectBaseUrl)
```

`res.locals` is an object in express that provides a way to pass data through the middleware and route handlers of your application. It's specifically scoped to the current request/response cycle.

* It persists only for the duration of the request-response cycle
* It's often used to pass data to view templates



Clone the repo
-----------------------------

You can clone the whole exercise repo like this.

```
git clone https://gitlab.com/mikael-roos/node.git
cd node
ls -al
```

This particular exercise lies in the directory `src/express5/login` and you need to move to that directory.

```
cd src/express5/login
ls -al
```



Init the database
-----------------------------

If you need to initiate the database from scratch, you can do so using the following scripts.

The script [`sql/create-user-maria.sql`](./sql/create-user-maria.sql) will create the database user that the examples use. You do not need to use this user, you can use another database user that you already have.

You need to be root user (or equal) to create a new database user.

```
mariadb -uroot -p < sql/create-user-maria.sql
```

The exampe program uses a database called `maria` for the examples. You can create that with the script [`sql/create-database.sql`](./sql/create-database.sql).

```
mariadb < sql/create-database.sql
```

This particular exercise uses a database table `user_jwt` that is created through the script [`sql/create-user-table.sql`](./sql/create-user-table.sql). 

```
mariadb maria < sql/create-user-table.sql
```



Start the server locally
-----------------------------

Install the dependencies:

```
npm install
```

Create your `.env` file by copying the `.env.example`.

```
cp .env.example .env
```

Review the file and edit its details if needed. It contain the essentials to connect to the database server so you need to adapt it to your local environment.

Start the server:

```
npm run dev
```

Open a web browser and connect to it using: `http://127.0.0.1:3000/`.

You can optionally start the server on another port like this.

```
PORT=3001 npm start
```



Init the database table
-----------------------------

You can list the existing users like this.

```
node script/jwt/list.js
```

The passwords in the database table is plain text passwords, you need to convert them into bcrypt hashed passwords. You can do that like this.

```
node script/jwt/init.js
```

Try list the users again to verify that all passwords are hashed passwords.



<!--
Start the server using docker compose
-----------------------------

You can build the image and start the container like this.

```
# Build the server
docker compose build server

# In the foreground
docker compose up server

# In the background, detached
docker compose up -d server
```

Open a web browser and connect to it using: `http://127.0.0.1:3000/`.

You can stop the container using `ctrl-c` (foreground) or with `docker compose down` (background).

-->
