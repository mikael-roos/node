import express from 'express'

export const router = express.Router()

router.get('/hello', async (req, res, next) => {
  res.send({
    message: 'Hello world!'
  })
})

router.post('/webhook', async (req, res, next) => {
  res.send('Thank you.')
  console.log(req.method)
  console.log(req.headers)
  console.log(req.body)
})
