import { app } from './src/express.js'

const server = app.listen(process.env.PORT || 3000, () => {
  console.log('App is up and running.')
})
