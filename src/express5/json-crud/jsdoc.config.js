export default {
  tags: {
    allowUnknownTags: true,
    dictionaries: ['jsdoc', 'closure']
  },
  sourceType: 'module',
  source: {
    include: ['src/controller', 'test'],
    includePattern: '.+\\.js(doc|x)?$'
  },
  opts: {
    destination: 'doc/',
    recurse: true,
    verbose: true
  }
}
