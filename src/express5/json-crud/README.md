---
revision: 
    "2025-02-03": "(B, mos) Updates after lecture."
    "2025-02-02": "(A, mos) First version."
---
![showff](.img/structure.png)

Exercise: Build a JSON REST API server for CRUD
==============================

This example shows how to perform CRUD (Create, Read, Update, Delete) towards a REST API server working with JSON.

You will be using the MVC design pattern (Model View Controller) to organise the code within the application server Express.

[[_TOC_]]

<!--
TODO

* Add jsdoc to eslint and use the fixer to generate
* Write a JavaScript main to perform JSON requests

-->



<!--
Video
-----------------------------

This is a recorded presentation, 18 minutes long (English), when Mikael goes through the content of this article.

[![2023-02-13 en](https://img.youtube.com/vi/wM1RPbxamC4/0.jpg)](https://www.youtube.com/watch?v=wM1RPbxamC4)
-->



The REST API
-----------------------------

You will implement the REST API towards a collection of users. You should be able to view all users, view one user, add and modify a user and delete a user from the collection.

Here is the REST API you should implement.

| Url path     | GET | POST | PUT | PATCH | DELETE |
|--------------|-----|------|-----|-------|--------|
| `/users`     | View all users | Add user |     |       | Remove all users |
| `users/{id}` | View user by id |      | Update the user object | Update details on user | Remove user |

Here is another way to review the REST API.

List all users.

```
GET /users
```

List a user with a id.

```
GET /users/{id}

GET /users/1
```

Add a user.

```
POST /users

# Send the user object as part of the body as json data
{
  "id": 2,
	"name": "Mikael Roos",
	"occupation": "programmer"
}
```

Update a user with new details (all details).

```
PUT /users/2

# Send the user object as part of the body as json data
{
  "id": 2,
	"name": "Mikael Roos",
	"occupation": "Awesome programmer"
}
```

Update a user with new details (some details).

```
PATCH /users/2

# Send the user object as part of the body as json data
{
	"occupation": "Awesome Magical programmer"
}
```

Delete all users.

```
DELETE /users
```

Delete a user with a id.

```
DELETE /users/{id}

DELETE /users/2
```



Work with Postman
-----------------------------

Install Postman so that you can work with it towards localhost.

Start by reading about how to [install Postman as an application on your own machine](https://learning.postman.com/docs/getting-started/installation/installation-and-updates/).

If you prefer the command line then take a look at [the cli tool newman](https://learning.postman.com/docs/collections/using-newman-cli/command-line-integration-with-newman/).



<!--
Write a JavaScript main to perform JSON requests
-----------------------------

-->



Add routes
-----------------------------

Now we need routes that support the REST API we need.

We start by adding a route file `src/route/users.js` and all the routes should mount on `/api/users`.

```js
import express from 'express'
import { controller } from '../controller/usersController.js'

export const router = express.Router()

router.param('id', controller.verifyUserId)

router.get('/users', controller.getUsers)
router.post('/users', controller.createUser)
router.get('/users/:id', controller.getUserById)
router.put('/users/:id', controller.updateUser)
router.patch('/users/:id', controller.partialUpdateUser)
router.delete('/users', controller.deleteUsers)
router.delete('/users/:id', controller.deleteUser)
```

Do not forget to include these routes in the `src/route/index.js` and mount them on `/api/v1`.

```js
import { router as usersRoute } from './users.js'

router.use('/api/v1', usersRoute)
```

We need the controller before we can test the code.



Add controller
-----------------------------

We add all the route handlers in a controller `src/controller/usersControllers.js`.

```js
let users = []

/**
 * Controller object for handling user-related operations.
 */
export const controller = {}

/**
 * Middleware to verify the user ID.
 * @param {Request} req - The request object.
 * @param {Response} res - The response object.
 * @param {function} next - The next middleware function.
 * @param {number} id - The user ID.
 */
controller.verifyUserId = (req, res, next, id) => {
  const userId = parseInt(id)
  if (!Number.isInteger(userId)) {
    return res.status(400).json({ error: 'Invalid ID format' })
  }
  req.userId = userId
  next()
}

/**
 * Get all users.
 * @param {Request} req - The request object.
 * @param {Response} res - The response object.
 */
controller.getUsers = (req, res) => {
  res.json(users)
}

/**
 * Create a new user.
 * @param {Request} req - The request object.
 * @param {Response} res - The response object.
 */
controller.createUser = (req, res) => {
  const user = req.body
  // Validering av användardata kan läggas till här
  users.push(user)
  res.status(201).json(user)
}

/**
 * Get a user by ID.
 * @param {Request} req - The request object.
 * @param {Response} res - The response object.
 */
controller.getUserById = (req, res) => {
  const user = users.find(u => u.id === req.userId)
  if (user) {
    res.json(user)
  } else {
    res.status(404).json({ error: 'User not found' })
  }
}

/**
 * Update a user by ID.
 * @param {Request} req - The request object.
 * @param {Response} res - The response object.
 */
controller.updateUser = (req, res) => {
  const user = users.find(u => u.id === req.userId)
  if (user) {
    Object.assign(user, req.body)
    res.json(user)
  } else {
    res.status(404).json({ error: 'User not found' })
  }
}

/**
 * Partially update a user by ID.
 * @param {Request} req - The request object.
 * @param {Response} res - The response object.
 */
controller.partialUpdateUser = (req, res) => {
  const user = users.find(u => u.id === req.userId)
  if (user) {
    Object.assign(user, req.body)
    res.json(user)
  } else {
    res.status(404).json({ error: 'User not found' })
  }
}

/**
 * Delete a user by ID.
 * @param {Request} req - The request object.
 * @param {Response} res - The response object.
 */
controller.deleteUser = (req, res) => {
  const index = users.findIndex(u => u.id === req.userId)
  if (index !== -1) {
    users.splice(index, 1)
    res.json({ result: 'User deleted' })
  } else {
    res.status(404).json({ error: 'User not found' })
  }
}

/**
 * Delete the users collection.
 * @param {Request} req - The request object.
 * @param {Response} res - The response object.
 */
controller.deleteUsers = (req, res) => {
  users = []
  res.json({ result: 'Users deleted' })
}
```

This controller partly depends on json data being sent in the request body so we need to configure the express server to deal with that in `src/express.js`.

```js
// Middleware to parse JSON data as part of the body
app.use(express.json())
```

Now we should be ready to run.



Verify that routes and the controller works
-----------------------------

Open Postman, or a similair tool, and verify that the routes and controllers works as expected.

Start with the GET routes.

```
GET /api/v1/users
```

```
GET /api/v1/users/1
```

Now try to add a user using POST.

```
POST /api/v1/users
```

Send the data in the body as json.

```json
{
  "id": 2,
	"name": "Mikael Roos",
	"occupation": "programmer"
}
```

Now try to update the user and change the user object using PUT.

```
PUT /api/v1/users/2
```

Send the user object in the body as json.

```json
{
  "id": 2,
	"name": "Mikael 'The Great' Roos",
	"occupation": "Awesome programmer"
}
```

Now try to update the user and change some details of the user object using PATCH.

```
PATCH /api/v1/users/2
```

Send the details you want to update in the body as json.

```json
{
	"occupation": "Awesome Magical programmer"
}
```

Delete a user with DELETE.

```
DELETE /api/v1/users/2
```

Delete all users.

```
DELETE /api/v1/users
```



Add model `UsersModel`
-----------------------------

To keep a thin controller we add all code that manages the storage of the users to the model class `UsersModel` in the file `src/model/UsersModel.js`.

Here is the template for the model class. The methods are empty so you can try to fill them in on your own. You can also opt to use the [full implementation of the class UsersModel](./src/model/UsersModel.js).

```js
/**
 * User model for handling user-related operations.
 */
class UsersModel {
  #users = [];

  /**
   * Verify and convert user ID.
   * @param {string} id - The user ID as a string.
   * @returns {number} - The verified user ID as an integer.
   * @throws {Error} - Throws an error if the ID format is invalid.
   */
  verifyUserId(id) {
  }

  /**
   * Get all users.
   * @returns {Array<Object>} - Array of all users.
   */
  getAllUsers() {
  }

  /**
   * Create a new user.
   * @param {Object} user - The user object to be created.
   * @returns {Object} - The created user object.
   */
  createUser(user) {
  }

  /**
   * Get a user by ID.
   * @param {number} userId - The ID of the user to retrieve.
   * @returns {Object|null} - The user object if found, otherwise null.
   */
  getUserById(userId) {
  }

  /**
   * Update a user by ID.
   * @param {number} userId - The ID of the user to update.
   * @param {Object} userData - The new data for the user.
   * @returns {Object|null} - The updated user object if found, otherwise null.
   */
  updateUser(userId, userData) {
  }

  /**
   * Delete a user by ID.
   * @param {number} userId - The ID of the user to delete.
   * @returns {boolean} - True if the user was deleted, otherwise false.
   */
  deleteUser(userId) {
  }

  /**
   * Delete all users.
   */
  deleteUsers() {
  }
}

export default new UsersModel();
```

Can you extend the model class with code so it implements the same features to the collection as before?

To be able to do that, you need to add this updated controller that uses the model class.

<details>
<summary>The version 2 of the controller that uses the UsersModel class</summary>

Save this as `src/controller/usersController_v2.js`.

```js
import UsersModel from '../model/UsersModel.js'

export const controller = {}

/**
 * Middleware to verify the user ID.
 * @param {Request} req - The request object.
 * @param {Response} res - The response object.
 * @param {function} next - The next middleware function.
 * @param {string} id - The user ID as a string.
 */
controller.verifyUserId = (req, res, next, id) => {
  try {
    req.userId = UsersModel.verifyUserId(id)
    next()
  } catch (error) {
    res.status(400).json({ error: error.message })
  }
}

/**
 * Get all users.
 * @param {Request} req - The request object.
 * @param {Response} res - The response object.
 */
controller.getUsers = (req, res) => {
  res.json(UsersModel.getAllUsers())
}

/**
 * Create a new user.
 * @param {Request} req - The request object.
 * @param {Response} res - The response object.
 */
controller.createUser = (req, res) => {
  const user = req.body
  const newUser = UsersModel.createUser(user)
  res.status(201).json(newUser)
}

/**
 * Get a user by ID.
 * @param {Request} req - The request object.
 * @param {Response} res - The response object.
 */
controller.getUserById = (req, res) => {
  const user = UsersModel.getUserById(req.userId)
  if (user) {
    res.json(user)
  } else {
    res.status(404).json({ error: 'User not found' })
  }
}

/**
 * Update a user by ID.
 * @param {Request} req - The request object.
 * @param {Response} res - The response object.
 */
controller.updateUser = (req, res) => {
  const user = UsersModel.updateUser(req.userId, req.body)
  if (user) {
    res.json(user)
  } else {
    res.status(404).json({ error: 'User not found' })
  }
}

/**
 * Partially update a user by ID.
 * @param {Request} req - The request object.
 * @param {Response} res - The response object.
 */
controller.partialUpdateUser = (req, res) => {
  const user = UsersModel.updateUser(req.userId, req.body)
  if (user) {
    res.json(user)
  } else {
    res.status(404).json({ error: 'User not found' })
  }
}

/**
 * Delete a user by ID.
 * @param {Request} req - The request object.
 * @param {Response} res - The response object.
 */
controller.deleteUser = (req, res) => {
  const result = UsersModel.deleteUser(req.userId)
  if (result) {
    res.json({ result: 'User deleted' })
  } else {
    res.status(404).json({ error: 'User not found' })
  }
}

/**
 * Delete all users.
 * @param {Request} req - The request object.
 * @param {Response} res - The response object.
 */
controller.deleteUsers = (req, res) => {
  UsersModel.deleteUsers()
  res.json({ result: 'Users deleted' })
}
```

</details>

You also need to use the controller through its routes and mount the new routes. 

<details>
<summary>Add the new routes for the version 2 of the controller.</summary>

Add this code to the file `src/route/users_v2.js`.

```js
import express from 'express'
import { controller } from '../controller/usersController_v2.js'

export const router = express.Router()

router.param('id', controller.verifyUserId)

router.get('/users', controller.getUsers)
router.post('/users', controller.createUser)
router.get('/users/:id', controller.getUserById)
router.put('/users/:id', controller.updateUser)
router.patch('/users/:id', controller.partialUpdateUser)
router.delete('/users', controller.deleteUsers)
router.delete('/users/:id', controller.deleteUser)
```

This code is the same as the other router file for the users, it just differs in what controller it uses.

</details>

Finally you need to mount the routes onto `/api/v2`.

<details>
<summary>Mount the new routes that uses the version 2 of the controller.</summary>

Add this to your `src/route/index.js`.

```js
import { router as usersRoute2 } from './users_v2.js'

router.use('/api/v2', usersRoute2)
```

</details>

Mounting these new routes onto `/api/v2` makes it possible to have both the old and the new code in the same server.

If you get into trouble, then [review the solution here](./src/).



Add documentation for your code using jsdoc
-----------------------------

We can generate a website with documentation of the source code by extracting the details from the jsdoc comments.

First we install the tools needed.

* [JSDoc](https://www.npmjs.com/package/jsdoc)
* [rimraf](https://www.npmjs.com/package/rimraf)

```
npm i jsdoc rimraf --save-dev
```

Add the following to the configuration file `jsdoc.config.json`.

```json
{
  "plugins": [],
  "recurseDepth": 10,
  "source": {
    "include": ["src", "test"],
    "includePattern": ".+\\.js(doc|x)?$",
    "excludePattern": "(^|\\/|\\\\)_"
  },
  "sourceType": "module",
  "tags": {
      "allowUnknownTags": true,
      "dictionaries": ["jsdoc", "closure"]
  },
  "templates": {
      "cleverLinks": false,
      "monospaceLinks": false
  },
  "opts": {
    "destination": "doc/",
    "recurse": true,
    "verbose": true
  }
}
```

To generate the docs we add the following to the script section.

```json
  "scripts": {
    "jsdoc": "npx jsdoc -c jsdoc.config.json"
  },
```

Now you can generate the documentation.

```
npm run jsdoc
```

Open your web browser to the directory `doc/` to review the documentation.

By the way, try to add jsdoc to all your files, it might be helpful. You can let your AI-friend add jsdoc to the code.




Add unit tests
-----------------------------

Now lets add unit tests to the code.


### Unit tests for the routes

You can add unit tests for the routes, like this in `test/route/users.test.js`.

```js
import { expect } from 'chai'
import request from 'supertest'
import express from 'express'
import { router} from '../../src/route/users_v2.js'

const app = express()
app.use(express.json())
app.use('/api', router)

describe('User Routes', () => {
  beforeEach(async () => {
    await request(app).delete('/api/users').send()
  })

  it('should create a new user', async () => {
    const res = await request(app)
      .post('/api/users')
      .send({ id: 1, name: 'John Doe' })
      .expect(201)

    expect(res.body).to.have.property('id', 1)
    expect(res.body).to.have.property('name', 'John Doe')
  })

  it('should get all users', async () => {
    await request(app).post('/api/users').send({ id: 1, name: 'John Doe' })

    const res = await request(app)
      .get('/api/users')
      .expect(200)

    expect(res.body).to.be.an('array').that.has.lengthOf(1)
    expect(res.body[0]).to.have.property('id', 1)
    expect(res.body[0]).to.have.property('name', 'John Doe')
  })
})
```

Try running the test suite.

<details>
<summary>More test cases for the routes.</summary>

```js
import { expect } from 'chai'
import request from 'supertest'
import express from 'express'
import { router} from '../../src/route/users_v2.js'

const app = express()
app.use(express.json())
app.use('/api', router)

describe('User Routes', () => {
  beforeEach(async () => {
    await request(app).delete('/api/users').send()
  })

  it('should create a new user', async () => {
    const res = await request(app)
      .post('/api/users')
      .send({ id: 1, name: 'John Doe' })
      .expect(201)

    expect(res.body).to.have.property('id', 1)
    expect(res.body).to.have.property('name', 'John Doe')
  })

  it('should get all users', async () => {
    await request(app).post('/api/users').send({ id: 1, name: 'John Doe' })

    const res = await request(app)
      .get('/api/users')
      .expect(200)

    expect(res.body).to.be.an('array').that.has.lengthOf(1)
    expect(res.body[0]).to.have.property('id', 1)
    expect(res.body[0]).to.have.property('name', 'John Doe')
  })

  it('should get a user by ID', async () => {
    await request(app).post('/api/users').send({ id: 1, name: 'John Doe' })

    const res = await request(app)
      .get('/api/users/1')
      .expect(200)

    expect(res.body).to.have.property('id', 1)
    expect(res.body).to.have.property('name', 'John Doe')
  })

  it('should update a user by ID', async () => {
    await request(app).post('/api/users').send({ id: 1, name: 'John Doe' })

    const res = await request(app)
      .put('/api/users/1')
      .send({ name: 'Jane Doe' })
      .expect(200)

    expect(res.body).to.have.property('name', 'Jane Doe')
  })

  it('should delete a user by ID', async () => {
    await request(app).post('/api/users').send({ id: 1, name: 'John Doe' })

    const res = await request(app)
      .delete('/api/users/1')
      .expect(200)

    expect(res.body).to.have.property('result', 'User deleted')
  })

  it('should delete all users', async () => {
    await request(app).post('/api/users').send({ id: 1, name: 'John Doe' })

    const res = await request(app)
      .delete('/api/users')
      .expect(200)

    expect(res.body).to.have.property('result', 'Users deleted')
  })
})
```

</details>



### Unit tests for the model

This is how unit tests can look for the model class.

Save the tests as `test/model/UsersModel.test.js`.

```js
import { expect } from 'chai'
import UsersModel from '../../src/model/UsersModel.js'

describe('UsersModel', () => {
  beforeEach(() => {
    UsersModel.deleteUsers()
  })

  it('should create a user', () => {
    const user = { id: 1, name: 'John Doe' }
    const newUser = UsersModel.createUser(user)
    expect(newUser).to.deep.equal(user)
  })
})
```

Try running the test suite.

<details>
<summary>More test cases for the model class.</summary>

```js
import { expect } from 'chai'
import UsersModel from '../../src/model/UsersModel.js'

describe('UsersModel', () => {
  beforeEach(() => {
    UsersModel.deleteUsers()
  })

  it('should create a user', () => {
    const user = { id: 1, name: 'John Doe' }
    const newUser = UsersModel.createUser(user)
    expect(newUser).to.deep.equal(user)
  })

  it('should get all users', () => {
    const user = { id: 1, name: 'John Doe' }
    UsersModel.createUser(user)
    const users = UsersModel.getAllUsers()
    expect(users).to.have.lengthOf(1)
    expect(users[0]).to.deep.equal(user)
  })

  it('should get a user by ID', () => {
    const user = { id: 1, name: 'John Doe' }
    UsersModel.createUser(user)
    const fetchedUser = UsersModel.getUserById(1)
    expect(fetchedUser).to.deep.equal(user)
  })

  it('should update a user by ID', () => {
    const user = { id: 1, name: 'John Doe' }
    UsersModel.createUser(user)
    const updatedUser = UsersModel.updateUser(1, { name: 'Jane Doe' })
    expect(updatedUser.name).to.equal('Jane Doe')
  })

  it('should delete a user by ID', () => {
    const user = { id: 1, name: 'John Doe' }
    UsersModel.createUser(user)
    const result = UsersModel.deleteUser(1)
    expect(result).to.be.true
    const users = UsersModel.getAllUsers()
    expect(users).to.have.lengthOf(0)
  })
})
```

</details>



### Unit tests for the controller

Testing the controller is similair to the routes, but we do not need to do an actual http request, we just call the controller method.

Here is how the test suite can look like in `test/controller/usersController.js`.

```js
import { expect } from 'chai'
import UsersModel from '../../src/model/UsersModel.js'
import { controller } from '../../src/controller/usersController.js'

describe('UserController', () => {
  let req, res

  beforeEach(() => {
    req = {}
    res = {
      json: (data) => res.body = data,
      status: (code) => {
        res.statusCode = code
        return res
      },
    }
    UsersModel.deleteUsers()
  })

  describe('getUsers', () => {
    it('should get all users', () => {
      controller.getUsers(req, res)
      expect(res.body).to.be.an('array').that.is.empty
    })
  })

  describe('createUser', () => {
    it('should create a new user', () => {
      req.body = { id: 1, name: 'John Doe' }
      controller.createUser(req, res)
      expect(res.statusCode).to.equal(201)
      expect(res.body).to.have.property('id', 1)
      expect(res.body).to.have.property('name', 'John Doe')
    })
  })
})
```

Try running the test suite.

<details>
<summary>More test cases for the controller.</summary>

```js
import { expect } from 'chai'
import UsersModel from '../../src/model/UsersModel.js'
import { controller } from '../../src/controller/usersController.js'

describe('UserController', () => {
  let req, res

  beforeEach(() => {
    req = {}
    res = {
      json: (data) => res.body = data,
      status: (code) => {
        res.statusCode = code
        return res
      },
    }
    UsersModel.deleteUsers()
  })

  describe('getUsers', () => {
    it('should get all users', () => {
      controller.getUsers(req, res)
      expect(res.body).to.be.an('array').that.is.empty
    })
  })

  describe('createUser', () => {
    it('should create a new user', () => {
      req.body = { id: 1, name: 'John Doe' }
      controller.createUser(req, res)
      expect(res.statusCode).to.equal(201)
      expect(res.body).to.have.property('id', 1)
      expect(res.body).to.have.property('name', 'John Doe')
    })
  })

  describe('getUserById', () => {
    it('should get a user by ID', () => {
      UsersModel.createUser({ id: 1, name: 'John Doe' })
      req.userId = 1
      controller.getUserById(req, res)
      expect(res.body).to.have.property('id', 1)
      expect(res.body).to.have.property('name', 'John Doe')
    })

    it('should return error if user not found', () => {
      req.userId = 999
      controller.getUserById(req, res)
      expect(res.statusCode).to.equal(404)
      expect(res.body).to.have.property('error', 'User not found')
    })
  })

  describe('updateUser', () => {
    it('should update a user by ID', () => {
      UsersModel.createUser({ id: 1, name: 'John Doe' })
      req.userId = 1
      req.body = { name: 'Jane Doe' }
      controller.updateUser(req, res)
      expect(res.body).to.have.property('name', 'Jane Doe')
    })

    it('should return error if user not found', () => {
      req.userId = 999
      req.body = { name: 'Jane Doe' }
      controller.updateUser(req, res)
      expect(res.statusCode).to.equal(404)
      expect(res.body).to.have.property('error', 'User not found')
    })
  })

  describe('deleteUser', () => {
    it('should delete a user by ID', () => {
      UsersModel.createUser({ id: 1, name: 'John Doe' })
      req.userId = 1
      controller.deleteUser(req, res)
      expect(res.body).to.have.property('result', 'User deleted')
    })

    it('should return error if user not found', () => {
      req.userId = 999
      controller.deleteUser(req, res)
      expect(res.statusCode).to.equal(404)
      expect(res.body).to.have.property('error', 'User not found')
    })
  })

  describe('deleteUsers', () => {
    it('should delete all users', () => {
      UsersModel.createUser({ id: 1, name: 'John Doe' })
      controller.deleteUsers(req, res)
      expect(res.body).to.have.property('result', 'Users deleted')
      expect(UsersModel.getAllUsers()).to.be.an('array').that.is.empty
    })
  })
})
```

</details>


<!--
Add code coverage
-----------------------------

When running unit tests we also want to monitor the code coverage. 

Install support for code coverage.

```js
npm install nyc --save-dev

npm install --save-dev babel-plugin-istanbul
```

Add the configuration file `.nycrc.json`.

```json
{
  "all": true,
  "reporter": ["text", "html"],
  "include": ["src/**/*.js"],
  "exclude": ["test/**/*.js"]
}
```

Add the script to run.

```json
  "scripts": {
    "coverage": "npx nyc npm test"
  },
```

-->
