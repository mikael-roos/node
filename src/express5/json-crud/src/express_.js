import express from 'express'
// import logger from 'morgan'
// import helmet from "helmet";
// import { router } from './route/index.js'
// import { handler as errorHandler } from './middleware/error_handler.js'

const app = express()

// Use the morgan logger
// app.use(logger('dev', { immediate: true }))

// Use Helmet
// app.use(helmet());

// Middleware to parse details of incoming requests
// app.use(express.urlencoded({ extended: true }))
// app.use(express.json())

// EJS view engine
// app.set('view engine', 'ejs')

// Use the public folder for static resources
// app.use(express.static('public'))

// Mount the routes
// app.use('/', router)

// Error handler
// app.use(errorHandler.notFoundDefault)
// app.use(errorHandler.errorDefault)

// Get the port number from the environment or use 3000 as default
export default (port = process.env.PORT || 3000) => {
  app.listen(port, () => {
    console.log(`Listening on port ${port}`)
  })
}

// // Enable the session
// app.use(session({
//   cookie: {
//     maxAge: 60000
//   },
//   resave: false,
//   saveUninitialized: true,
//   secret: 'keyboard cat'
// }))
