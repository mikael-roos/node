import express from 'express'
import { db } from './database.js'

export const router = express.Router()

router.get('/hello', (req, res, next) => {
  res.json({
    message: 'Hello world!'
  })
})

router.post('/body', (req, res, next) => {
  console.log(req.headers)
  res.json({
    message: 'I got the following body!',
    body: req.body
  })
})

router.get('/users', async (req, res, next) => {
  const sql = 'SELECT * FROM user'
  const [rows] = await db.execute(sql)
  res.json(rows)
})

router.get('/users/search', async (req, res, next) => {
  let search = req.query?.search
  search = `%${search}%`
  
  const sql = `
  SELECT * FROM user
  WHERE username LIKE ? OR email LIKE ?
  `
  const [rows] = await db.execute(sql, [search, search])
  res.json(rows)
})

router.get('/users/:id', async (req, res, next) => {
  const sql = 'SELECT * FROM user WHERE id = ?'
  const [rows] = await db.execute(sql, [req.params.id])
  res.json(rows)
})

router.post('/users', async (req, res, next) => {
  const username = req.body.username
  const password = req.body.password
  const email = req.body.email

  const sql = `
  INSERT INTO user 
    (username, password, email)
  VALUES
    (?, ?, ?)
  `
  const [data] = await db.execute(sql, [username, password, email])

  // console.log(data)
  res.status(201).json({
    "id": data.insertId
  })
})

router.put('/users/:id', async (req, res, next) => {
  const id = req.params.id
  const username = req.body.username
  const password = req.body.password
  const email = req.body.email

  const sql = `
  UPDATE user SET 
    username = ?,
    password = ?,
    email = ?
  WHERE id = ?
  `
  const [data] = await db.execute(sql, [username, password, email, id])
  //console.log(data)
  res.status(204).send()
})

router.patch('/users/:id', async (req, res, next) => {
  const id = req.params.id
  const password = req.body.password

  const sql = `
  UPDATE user SET 
    password = ?
  WHERE id = ?
  `
  const [data] = await db.execute(sql, [password, id])
  //console.log(data)
  res.status(204).send()
})

router.delete('/users/:id', async (req, res, next) => {
  const id = req.params.id
  const sql = `
  DELETE FROM user
  WHERE id = ?
  `
  const [data] = await db.execute(sql, [id])
  //console.log(data)
  res.status(204).send()
})
