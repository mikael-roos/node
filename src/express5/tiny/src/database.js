import mysql from 'mysql2/promise'

export const db = await mysql.createConnection({
  host:     process.env.DB_HOST     || 'localhost',
  user:     process.env.DB_USER     || 'maria',
  password: process.env.DB_PASSWORD || 'M@aria',
  database: process.env.DB_SCHEMA   || 'maria'
})
