/**
 * Work with JavaScript builtin objects in Node.
 */
const dice = Math.round(Math.random() * 6 + 1)
console.log(`The dice is ${dice}`)

const date = new Date()
console.log(date)

const object = {
  key1: 'Value 1',
  key2: 42
}
console.log(JSON.stringify(object, null, 2))
