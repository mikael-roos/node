---
revision: 
    "2025-02-02": "(A, mos) First version."
---
<!-- ![header](.img/structure.png) -->

Exercise: Docker as a production server with Nginx, https, http2, and pm2 with MongoDB
==============================

This example simulates how to build aup a production server for node express applications using nginx as front.

[[_TOC_]]

<!--
TODO

* 

-->



<!--
Video
-----------------------------

This is a recorded presentation, 18 minutes long (English), when Mikael goes through the content of this article.

[![2023-02-13 en](https://img.youtube.com/vi/wM1RPbxamC4/0.jpg)](https://www.youtube.com/watch?v=wM1RPbxamC4)
-->



Install express
-----------------------------

The aim is to install Express and configure it as an application server that says "Hello World". While doing 
