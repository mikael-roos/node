---
revision: 
    2025-02-16: "(C, mos) Walkthrough and general improvements."
    2024-03-04: "(B, mos) Walkthrough and general improvements."
    2023-01-31: "(A, mos) First version."
---
Lecture: Web application security
========================

[![presentation image](./img/slide.png)](https://mikael-roos.gitlab.io/node/lecture/L12-security/slide.html)

This lecture is about web application security. We dig into the OWASP Top Ten security issues to learn more about the most common security flaws. We see how this relates to the lists "Common Weakness Enumeration (CWE)" and "Common Vulnerabilities and Exposures (CVE)".

We look through some examples of common attacks and flaws to see how to protect ourselves and the application server.

The "OWASP Juice Shop" is introduced as a playground to learn more about the security aspects of a web application.

You can browse [the HTML slides used in the presentation](https://mikael-roos.gitlab.io/node/lecture/L12-security/slide.html) (press f/esc to enter/exit full screen mode, navigate using the arrow keys, use `ctrl -`/`ctrl +` to resize the slides).

<!--
Recorded presentation, 34 minutes long (English).

[![2023-01-31 en](https://img.youtube.com/vi/WO4vtwj3Hec/0.jpg)](https://www.youtube.com/watch?v=WO4vtwj3Hec)
-->



About OWASP, CWE and CVE
------------------------

OWASP, CWE, and CVE work together to enhance software security, each with a distinct focus. OWASP highlights the most critical security risks, such as those listed in the OWASP Top 10, to help developers and organizations prioritize their efforts. CWE provides a detailed catalog of software weaknesses, explaining the root causes of vulnerabilities and offering guidance on how to prevent them. CVE focuses on specific, real-world vulnerabilities, assigning each a unique identifier to make them easier to track and address. Together, these frameworks provide a comprehensive approach to understanding, preventing, and mitigating security risks in software.



Learn more
------------------------

The "OWASP Juice Shop" is a insecure web application that can be used for security trainings and awareness demos. Juice Shop encompasses vulnerabilities from the OWASP Top Ten (and more).

* [OWASP Juice Shop](https://owasp.org/www-project-juice-shop/)

It is easy to get going with the Juice shop if you user docker.

```
docker run --rm -p 3000:3000 bkimminich/juice-shop
```

Follow the hints on the user interface and when you find the score board you can start with the tutorial exercises which also have a visual guideance on how to solve them.

<!--
* How to attack a web app
* How to secure a web app

Perhaps add more training through https://www.hacksplaining.com/ and the book.

Or checkout this: https://tryhackme.com/

-->



Resources
------------------------

Resources that are used, or are relevant, for the areas that the lecture covers.

* [OWASP Top Ten](https://owasp.org/www-project-top-ten/)
* [Common Weakness Enumeration (CWE)](https://cwe.mitre.org/)
    * [CWE Top 25](https://cwe.mitre.org/top25/archive/2022/2022_cwe_top25.html)
* [Common Vulnerabilities and Exposures (CVE)](https://www.cve.org/)
