---
revision: 
    2024-03-10: "(A, mos) first version"
---
Lecture: Authentication and authorization
========================

[![presentation image](./img/slide.png)](https://mikael-roos.gitlab.io/node/lecture/L0X-authentication-authorization/slide.html)

This lecture is about authentication and authorization in a web application. We explain the terms authentication and authorization and we see how different (access versus role-based) control lists can be used to manage who can access what resource. 

The secrets need to be securely stored, both in the database and in the repository.

We wrap up by reviewing the technique for OAuth, API keys and JSON Web Token.

You can browse [the HTML slides used in the presentation](https://mikael-roos.gitlab.io/node/lecture/L0X-authentication-authorization/slide.html) (press f/esc to enter/exit full screen mode, navigate using the arrow keys, use `ctrl -`/`ctrl +` to resize the slides).

<!--
TODO

* Update above intro text and references below since the slide has more content.
* Update the "Learn more" section with API and JWT exercise, perhaps also session and authentication (web) exercises.

-->


<!--

Recorded presentation, 24 minutes long (English).

[![2023-02-13 en](https://img.youtube.com/vi/ePBbXm_tTVo/0.jpg)](https://www.youtube.com/watch?v=ePBbXm_tTVo)
-->


Learn more
------------------------

<!--
TODO

* Add bcrypt exercise
* login/logout exercise
* work with session
* flash messages
* update navbar when user is loggedin
* use roles to protect some pages

---

The code used in the slides are available in the following exercises.

* [`src/express/session`](../../../src/express/session/).
* [`src/express/authentication`](../../../src/express/authentication/).

Work through the exercises to get a live and running example together with an understanding on how to implement sessions, flash messages and authetication.
-->

Short article on "[How To Safely Store A Password](https://codahale.com/how-to-safely-store-a-password/)".



Resources
------------------------

Resources that are used, or are relevant, for the areas that the lecture covers.

* [Wikipedia Authentication](https://en.wikipedia.org/wiki/Authentication)
* [Wikipedia Authorization](https://en.wikipedia.org/wiki/Authorization)
* [Wikipedia Access-control list](https://en.wikipedia.org/wiki/Access-control_list)
* [Wikipedia Role-based access control](https://en.wikipedia.org/wiki/Role-based_access_control)
* [Wikipedia on Bcrypt algorithm](https://en.wikipedia.org/wiki/Bcrypt)
    * [npm package bcrypt](https://www.npmjs.com/package/bcrypt)
* [About .env (Ruby gem)](https://github.com/bkeepers/dotenv)

<!--
* [Express](https://expressjs.com/)
    * [npm package express-session](https://www.npmjs.com/package/express-session)
-->
