---
revision: 
    2023-01-31: "(A, mos) first version"
---
Lecture: Microservices, architecture and techniques
========================

[![presentation image](./img/slide.png)](https://mikael-roos.gitlab.io/node/lecture/L0X-micro-services/slide.html)

This lecture is about architecture of web applications and related technique to build web applications and services.

* Micro services
* Serverless architecture
* Web sockets
* Server-send events
* Web hooks

You can browse [the HTML slides used in the presentation](https://mikael-roos.gitlab.io/node/lecture/L0X-micro-services/slide.html) (press f/esc to enter/exit full screen mode and navigate using the arrow keys).

<!--
Recorded presentation, 34 minutes long (English).

[![2023-01-31 en](https://img.youtube.com/vi/WO4vtwj3Hec/0.jpg)](https://www.youtube.com/watch?v=WO4vtwj3Hec)
-->



<!--
Learn more
------------------------

The "OWASP Juice Shop" is a insecure web application that can be used for security trainings and awareness demos. Juice Shop encompasses vulnerabilities from the OWASP Top Ten (and more).

* [OWASP Juice Shop](https://owasp.org/www-project-juice-shop/)

It is easy to get going with the Juice shop if you user docker.

```
docker run --rm -p 3000:3000 bkimminich/juice-shop
```
-->



Resources
------------------------

Resources that are used, or are relevant, for the areas that the lecture covers.

* [Wikipedia Microservices](https://en.wikipedia.org/wiki/Microservices)
